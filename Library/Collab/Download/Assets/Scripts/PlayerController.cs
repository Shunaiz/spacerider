﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [Range(1, 15)] //allows adjusting move speed while testing the game to get a specific feel
    public float walkSpeed = 7.0f; //default value feels ok

    [Range(1, 20)] //allows adjusting jump height while testing the game to get a specific feel
    public float jumpSpeed = 10.0f; //default value feels ok

    [Range (1, 10)] //allows adjusting fall multiplier while testing the game to get a specific feel
    public float fallMultiplier = 2.5f; //default value feels ok

    [Range(1, 10)] //allows adjusting fall multiplier while testing the game to get a specific feel
    public float lowJumpModifier = 5.0f; //default value feels ok

    [Range(0.2f, 3f)]
    public float fireRate; //placeholder for fire rate - should be weapon dependent in the future

    private bool pressedJump;

    public LayerMask ground;
    public Transform foot1;
    //public AudioSource audio;
    public Transform bulletPrefab;
    //==================================== needed for source shooter
    public Transform material;
    public Transform PistolMatPre;
    public Transform shotgunMatPre;
    public Transform bigshotMatPre;
    public Transform threeRoundBurstMatPre;
    public Transform fullAutoMatPre;
    public Transform rocketMatPre;
    //====================================
    public Transform pistolPrefab; //pistol
    public Transform threeRoundPrefab; //three round burst
    public Transform shotgunPrefab; //shotgun
    public Transform heavyPrefab; //heavy shot
    public Transform fullAutoPrefab; //FullAuto
    public Transform rocketPrefab; //Rocket
    public AudioClip PlayerJump;
    public AudioSource PlayerSource;
    private GameObject firePosition;
    private Rigidbody rb;
    private CapsuleCollider playerCollider;
    private float timeElapsed;
    private bool tempUnlock;
    Transform FirePos;
    MovingPlatform_1 currentPlatform;


    //bool types for different weapons
    private bool pistol = true;
    private bool shotgun = false;
    private bool bigShot = false;
    private bool threeRoundBurst = false;
    private bool fullAuto = false;
    private bool rocket = false;
    //================================
    //upgrades required to use them must find.
    static bool shotgunMat = false;
    static bool bigShotMat = false;
    static bool threeRoundBurstMat = false;
    static bool fullAutoMat = false;
    static bool rocketMat = false;
    //================================
    public RawImage pistolIcon;
    public RawImage shotgunIcon;
    public RawImage bigShotIcon;
    public RawImage threeRoundBurstIcon;
    public RawImage fullAutoIcon;
    public RawImage rocketIcon;
    //================================
    
    private Vector3 direction;

    public static bool faceRight = true;
    private bool shotBehind = false;

    private Animator anim;
    private int a_move_hash = Animator.StringToHash("speed");
    private int a_jump_hash = Animator.StringToHash("jump");
    private int a_crouch_hash = Animator.StringToHash("crouch");
    private int a_uncrouch_hash = Animator.StringToHash("uncrouch");

    void Awake()
    {
        FirePos = transform.Find("FirePos");
        if (FirePos == null)
        {
            Debug.LogError("No Fireposition");
        }
    }

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.interpolation = RigidbodyInterpolation.Interpolate; //smooths out player movement

        playerCollider = GetComponent<CapsuleCollider>();
        pressedJump = false;
        direction = Vector3.zero;
        anim = GetComponent<Animator>();
        firePosition = GameObject.FindGameObjectWithTag("firepos");
        timeElapsed = 0f;
        currentPlatform = null;
        pistolIcon.enabled = true;
        shotgunIcon.enabled = false;
        bigShotIcon.enabled = false;
        threeRoundBurstIcon.enabled = false;
        fullAutoIcon.enabled = false;
        rocketIcon.enabled = false;
}

    // Update is called once per frame
    void Update()
    {
        //Debug.DrawRay(transform.position, transform.forward);

        if (PauseMenu.IsPaused() || MissionSelect.IsPaused() || ShopScript.IsPaused()) return;

        timeElapsed += Time.deltaTime;

        SetWeaponHandler();
        FireHandler();
        CrouchHandler();
    }

    void FixedUpdate()
    {
        //All movement handling is done in FixedUpdate to make the physics smoother
        MoveHandler();
        JumpHandler();
        
    }


    void FireProjectile()
    {   
        Vector2 mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        Vector2 firePointPosition = new Vector2(FirePos.position.x, FirePos.position.y);
        //RaycastHit2D hit = Physics2D.Raycast(firePointPosition, mousePosition - firePointPosition, 100, whatToHit); //the direction
        
        Vector2 spawnLocation = firePointPosition;

        float AngleRad = Mathf.Atan2(mousePosition.y - transform.position.y, mousePosition.x - transform.position.x);

        float AngleDeg = (180 / Mathf.PI) * AngleRad;
        //Debug.Log(AngleDeg);

        if (AngleDeg < 90 && AngleDeg > -90 && faceRight)
        {
            //no change
            shotBehind = false;
        }
        else if(AngleDeg < -90 && AngleDeg > 90 && !faceRight)
        {
            shotBehind = false;
        }
        else if((AngleDeg > 90 || AngleDeg < -90) && faceRight)
        {
            //Debug.Log("Shot behind facing right");
            spawnLocation = new Vector2(FirePos.position.x - 1.3f, FirePos.position.y);
            //Debug.Log("new spawnLoc is " + spawnLocation);
            shotBehind = true;
        }
        else if(AngleDeg > -90 && AngleDeg < 90 && !faceRight)
        {
            //Debug.Log("Shot behind facing left");
            spawnLocation = new Vector2(FirePos.position.x + 1.3f, FirePos.position.y);
            shotBehind = true;
        }

        //if (hit.collider != null)
        //{
        //Debug.DrawLine(firePointPosition, hit.point, Color.red);
        //}
        //Debug.Log("We hit" + hit.collider.name + "and did" + damage + "damage,");

        Debug.DrawLine(spawnLocation, (mousePosition - firePointPosition) * 100);

        AngleRad = Mathf.Atan2(mousePosition.y - spawnLocation.y, mousePosition.x - spawnLocation.x);

        AngleDeg = (180 / Mathf.PI) * AngleRad;

        Instantiate(material, spawnLocation, Quaternion.Euler(0, 0, (AngleDeg - 90)));
        Instantiate(bulletPrefab, spawnLocation, Quaternion.Euler(0, 0, (AngleDeg - 90)));

    }

    void MoveHandler()
    {
        // Set x and z velocities to zero
        rb.velocity = new Vector3(0, rb.velocity.y, 0);

        // Distance ( speed = distance / time --> distance = speed * time)
        float distance = walkSpeed * Time.deltaTime;

        // Input on x ("Horizontal")
        float hAxis = Input.GetAxis("Horizontal");

        //get direction for model facing
        direction.x = hAxis;
        direction = direction.normalized;

        // Movement vector
        Vector3 movement = new Vector3(hAxis * distance, 0f, 0f);

        // Current position
        Vector3 currPosition = transform.position;

        // New position
        Vector3 newPosition = currPosition + movement;

        // Move the rigid body
        rb.MovePosition(newPosition);

        //handle facing and animation parameters
        if (direction != Vector3.zero)
        {
            transform.forward = direction;
            anim.SetFloat(a_move_hash, walkSpeed);

            if (direction.x > 0)
                faceRight = true;
            else
                faceRight = false;
            //Debug.Log(direction);
            //Debug.Log(move);
        }
        else
        {
            anim.SetFloat(a_move_hash, -1.0f);

            if (currentPlatform)
            {
                MovingPlatformHandler();
            }
        }

        
    }

    void JumpHandler()
    {
        // Jump axis
        float jAxis = Input.GetAxis("Jump");
        //Debug.Log("rb.y velocity = " + rb.velocity.y);

        bool isGrounded = Physics.CheckSphere(foot1.position, 0.2f, ground, QueryTriggerInteraction.Ignore);

        // Check if the player is pressing the jump key
        if (jAxis > 0f)
        {
            // Make sure we've not already jumped on this key press
            if (!pressedJump && isGrounded)
            {
                // We are jumping on the current key press
                pressedJump = true;
                PlayerSource.clip = PlayerJump;
                AudioSource.PlayClipAtPoint(PlayerSource.clip, transform.position);
                anim.SetTrigger(a_jump_hash);

                // Jumping vector
                Vector3 jumpVector = new Vector3(0f, jumpSpeed, 0f);

                // Make the player jump by adding velocity
                rb.velocity = rb.velocity + jumpVector;

                //Debug.Log("rb.velocity set to " + rb.velocity);
            }
        }
        else
        {
            // Update flag so it can jump again if we press the jump key
            pressedJump = false;
        }


        if (!Input.GetKey(KeyCode.Space) && rb.velocity.y > 0) //while user is not holding down the space bar after jumping, he will fall faster (short jump)
        {         
            rb.velocity += Physics.gravity * Time.deltaTime * (lowJumpModifier - 1);
        }
        else if(rb.velocity.y < -1f && !isGrounded) //else if the user is starting to fall, add the normal fall multiplier
        {
            rb.velocity += Physics.gravity * Time.deltaTime * (fallMultiplier - 1);
        }
    }

    void FireHandler()
    {
        if(timeElapsed > fireRate && pistol == true)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                FireProjectile();
                timeElapsed = 0f;
            }
        }
        if (timeElapsed > fireRate && threeRoundBurst ==true)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                StartCoroutine("WaitExample");
                timeElapsed = 0f;
            }
        }
        if (timeElapsed > fireRate && shotgun == true)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                FireProjectile();
                FireProjectile();
                FireProjectile();
                FireProjectile();
                FireProjectile();
                timeElapsed = 0f;
            }
        }
        if (timeElapsed > fireRate && bigShot == true)//need dif prefab bullet
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                FireProjectile();
                timeElapsed = 0f;
            }
        }
        if (fullAuto == true)//need dif prefab bullet
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                FireProjectile();
                timeElapsed = 0f;
            }
        }
        if (timeElapsed > fireRate && rocket == true)//need dif prefab bullet
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                FireProjectile();
                timeElapsed = 0f;
            }
        }
        else return;
    }

    void CrouchHandler()
    {
        //Crouching
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            Debug.Log("Crouched");
            anim.SetTrigger(a_crouch_hash);
            firePosition.transform.position += new Vector3(0f, -0.5f, 0f);
        }
        else if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            Debug.Log("Uncrouched");
            anim.SetTrigger(a_uncrouch_hash);
            firePosition.transform.position += new Vector3(0f, 0.5f, 0f);
        }

    }

    void MovingPlatformHandler()
    {
        if(Input.GetAxis("Horizontal") < Mathf.Epsilon) //if player is not actively moving on the platform
        {
            if (currentPlatform.movingRight) //moving right
            {
                if(!faceRight) //this facing check is because the player is moved in relation to his forward/backward vector from his transform
                    transform.Translate(Vector3.back * currentPlatform.speed * Time.deltaTime); //move the player right with the platform
                else
                    transform.Translate(Vector3.forward * currentPlatform.speed * Time.deltaTime); //move the player right with the platform
            }
            else if (!currentPlatform.movingRight) //moving left
            {
                if(!faceRight) //this facing check is because the player is moved in relation to his forward/backward vector from his transform
                    transform.Translate(Vector3.forward * currentPlatform.speed * Time.deltaTime); //move the player left with the platform
                else
                    transform.Translate(Vector3.back * currentPlatform.speed * Time.deltaTime); //move the player left with the platform
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("MovingPlatform"))
        {
            currentPlatform = collision.gameObject.GetComponent<MovingPlatform_1>();
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("MovingPlatform"))
        {
            currentPlatform = null;
        }
    }

    private void SetWeaponHandler()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Debug.Log("Pistol On");
            //flags to switch weapon fire type
            pistol = true;
            shotgun = false;
            bigShot = false;
            threeRoundBurst = false;
            fullAuto = false;
            rocket = false;
            //flags to switch weapon fire typp Icon
            pistolIcon.enabled = true;
            shotgunIcon.enabled = false;
            bigShotIcon.enabled = false;
            threeRoundBurstIcon.enabled = false;
            fullAutoIcon.enabled = false;
            rocketIcon.enabled = false;
            //switch bullet prefab instated
            material = PistolMatPre;
            bulletPrefab = pistolPrefab;

        }
        if (Input.GetKeyDown(KeyCode.Alpha2) && threeRoundBurstMat == true) //add mat requirement for real game
        {
            Debug.Log("Three Round Burst On");
            //flags to switch weapon fire type
            pistol = false;
            shotgun = false;
            bigShot = false;
            threeRoundBurst = true;
            fullAuto = false;
            rocket = false;
            //flags to switch weapon fire typp Icon
            pistolIcon.enabled = false;
            shotgunIcon.enabled = false;
            bigShotIcon.enabled = false;
            threeRoundBurstIcon.enabled = true;
            fullAutoIcon.enabled = false;
            rocketIcon.enabled = false;
            //switch bullet prefab instated
            material = threeRoundBurstMatPre;
            bulletPrefab = threeRoundPrefab;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3) && shotgunMat == true) //add mat requirement for real game
        {
            Debug.Log("Shotgun On");
            //flags to switch weapon fire type
            pistol = false;
            shotgun = true;
            bigShot = false;
            threeRoundBurst = false;
            fullAuto = false;
            rocket = false;
            //flags to switch weapon fire typp Icon
            pistolIcon.enabled = false;
            shotgunIcon.enabled = true;
            bigShotIcon.enabled = false;
            threeRoundBurstIcon.enabled = false;
            fullAutoIcon.enabled = false;
            rocketIcon.enabled = false;
            //switch bullet prefab instated
            material = shotgunMatPre;
            bulletPrefab = shotgunPrefab;

        }
        if (Input.GetKeyDown(KeyCode.Alpha4) && bigShotMat == true) //add mat requirement for real game
        {
            Debug.Log("BigShot On");
            //flags to switch weapon fire type
            pistol = false;
            shotgun = false;
            bigShot = true;
            threeRoundBurst = false;
            fullAuto = false;
            rocket = false;
            //flags to switch weapon fire typp Icon
            pistolIcon.enabled = false;
            shotgunIcon.enabled = false;
            bigShotIcon.enabled = true;
            threeRoundBurstIcon.enabled = false;
            fullAutoIcon.enabled = false;
            rocketIcon.enabled = false;
            //switch bullet prefab instated
            material = bigshotMatPre;
            bulletPrefab = heavyPrefab;
        }
        if (Input.GetKeyDown(KeyCode.Alpha5) && fullAutoMat == true) //add mat requirement for real game
        {
            Debug.Log("Full Auto On");
            //flags to switch weapon fire type
            pistol = false;
            shotgun = false;
            bigShot = false;
            threeRoundBurst = false;
            fullAuto = true;
            rocket = false;
            //flags to switch weapon fire typp Icon
            pistolIcon.enabled = false;
            shotgunIcon.enabled = false;
            bigShotIcon.enabled = false;
            threeRoundBurstIcon.enabled = false;
            fullAutoIcon.enabled = true;
            rocketIcon.enabled = false;
            //switch bullet prefab instated
            material = fullAutoMatPre;
            bulletPrefab = fullAutoPrefab;
        }
        if (Input.GetKeyDown(KeyCode.Alpha6) && rocketMat == true) //add mat requirement for real game
        {
            Debug.Log("Rocketo On");
            //flags to switch weapon fire type
            pistol = false;
            shotgun = false;
            bigShot = false;
            threeRoundBurst = false;
            fullAuto = false;
            rocket = true;
            //flags to switch weapon fire typp Icon
            pistolIcon.enabled = false;
            shotgunIcon.enabled = false;
            bigShotIcon.enabled = false;
            threeRoundBurstIcon.enabled = false;
            fullAutoIcon.enabled = false;
            rocketIcon.enabled = true;
            //switch bullet prefab instated
            material = rocketMatPre;
            bulletPrefab = rocketPrefab;
        }
    }

    IEnumerator WaitExample()
    {
        FireProjectile();
        yield return new WaitForSecondsRealtime(.1f);
        FireProjectile();
        yield return new WaitForSecondsRealtime(.1f);
        FireProjectile();
    }

    //checks for save data
    public bool ThreeRoundMatCheck()
    {
        return threeRoundBurstMat;
    }
    public bool ShotgunMatCheck()
    {
        return shotgunMat;
    }
    public bool HeavyMatCheck()
    {
        return bigShotMat;
    }
    public bool FullAutoMatCheck()
    {
        return fullAutoMat;
    }
    public bool RocketMatCheck()
    {
        return rocketMat;
    }

    //==========Set and unset functions for guns===========

    public void SetFullAutoAllow()
    {
        fullAutoMat = true;
    }

    public void SetShotgunAllow()
    {
        shotgunMat = true;
    }
    public void SetShotgunFail()
    {
        shotgunMat = false;
    }

    public void SetTrbAllow()
    {
        threeRoundBurstMat = true;
    }
    public void SetTrbFail()
    {
        threeRoundBurstMat = false;
    }

    public void SetHeavyAllow()
    {
        bigShotMat = true;
    }
    public void SetHeavyFail()
    {
        bigShotMat = false;
    }

    public void SetRocketAllow()
    {
        rocketMat = true;
    }
    public void SetRocketFail()
    {
        rocketMat = false;
    }

    //========Set and unset functions for guns end=========

    //save function & load function
    public void SaveGun()
    {
        GunSaveSystem.SaveGun(this);
    }

    public void LoadGun()
    {
        GunData data = GunSaveSystem.LoadGun();

        threeRoundBurstMat = data.threeRoundMat;
        shotgunMat = data.shotgunMat;
        bigShotMat = data.heavyMat;
        fullAutoMat = data.fullAutoMat;
        rocketMat = data.rocketMat;
    }
}
