﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopScript : MonoBehaviour
{
    private int buttonWidth;
    private int buttonHeight;
    private int origin_x;
    private int origin_y;
    public GUISkin skin;
    public GameObject player;

    private static bool isPaused = false;

    // Use this for initialization
    void Start()
    {
        buttonWidth = 600;
        buttonHeight = 25;
        origin_x = Screen.width / 2 - buttonWidth / 2;
        origin_y = Screen.height / 2 - buttonHeight * 2;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
            isPaused = true;
    }

    void OnGUI()
    {
        GUI.skin = skin;// Resources.Load<GUISkin>("Materials and Textures/UI/UISkin");

        if (isPaused)
        {
            if (GUI.Button(new Rect(origin_x, origin_y, buttonWidth, buttonHeight), "Buy Life ... 10,000"))
            { //check if player has enough money allow if not no deal
                if (player.GetComponent<Health_O2_Music>().MoneyCheck() >= 10000)
                {
                    player.GetComponent<Health_O2_Music>().AddLife();
                    player.GetComponent<Health_O2_Music>().SubtractMoney(10000);
                    player.GetComponent<Health_O2_Music>().SavePlayer();
                }
                else
                {
                    //do nothing
                    Debug.Log("Not Enough Money");
                }
            }
            if (GUI.Button(new Rect(origin_x, origin_y + buttonHeight * 1 + 5, buttonWidth, buttonHeight), "Buy Health Upgrade (+25) ... 250,000"))
            { //check if player has enough money allow if not no deal
                if (player.GetComponent<Health_O2_Music>().MoneyCheck() >= 250000)
                {
                    player.GetComponent<Health_O2_Music>().HealthUpgrade();
                    player.GetComponent<Health_O2_Music>().SubtractMoney(250000);
                    player.GetComponent<Health_O2_Music>().SavePlayer();
                    player.GetComponent<PlayerController>().SaveGun();
                }
                else
                {
                    //do nothing
                    Debug.Log("Not Enough Money");
                }
            }
            if (GUI.Button(new Rect(origin_x, origin_y + buttonHeight * 2 + 10, buttonWidth, buttonHeight), "Buy Oxygen Upgrade (+25) ... 250,000"))
            { //check if player has enough money allow if not no deal
                if (player.GetComponent<Health_O2_Music>().MoneyCheck() >= 250000)
                {
                    player.GetComponent<Health_O2_Music>().OxygenUpgrade();
                    player.GetComponent<Health_O2_Music>().SubtractMoney(250000);
                    player.GetComponent<Health_O2_Music>().SavePlayer();
                    player.GetComponent<PlayerController>().SaveGun();
                }
                else
                {
                    //do nothing
                    Debug.Log("Not Enough Money");
                }
            }
            if (GUI.Button(new Rect(origin_x, origin_y + buttonHeight * 3 + 15, buttonWidth, buttonHeight), "Buy Full-Auto ... 1,000,000"))
            { //check if player has enough money allow if not no deal
                if (player.GetComponent<Health_O2_Music>().MoneyCheck() >= 1000000)
                {
                    // give gun player.GetComponent<Health_O2_Music>().AddLife();
                    player.GetComponent<PlayerController>().SetFullAutoAllow();
                    player.GetComponent<Health_O2_Music>().SubtractMoney(1000000);
                    player.GetComponent<Health_O2_Music>().SavePlayer();
                    player.GetComponent<PlayerController>().SaveGun();
                }
                else
                {
                    //do nothing
                    Debug.Log("Not Enough Money");
                }
            }
            if (GUI.Button(new Rect(origin_x, origin_y + buttonHeight * 4 + 20, buttonWidth, buttonHeight), "Buy Familys Freedom ... 1,000,000,000"))
            {
                //make it go to end game... idk
                if (player.GetComponent<Health_O2_Music>().MoneyCheck() >= 1000000000)
                {
                    // give gun player.GetComponent<Health_O2_Music>().AddLife();
                    player.GetComponent<Health_O2_Music>().SubtractMoney(1000000000);
                    player.GetComponent<Health_O2_Music>().SavePlayer();
                    player.GetComponent<PlayerController>().SaveGun();
                }
                else
                {
                    //do nothing
                    Debug.Log("Not Enough Money");
                }
            }
            if (GUI.Button(new Rect(origin_x, origin_y + buttonHeight * 5 + 25, buttonWidth, buttonHeight), "Exit Shop"))
            {
                isPaused = false;
            }
        }

    }

    public static bool IsPaused()
    {
        if (isPaused)
            return true;
        else
            return false;
    }
}