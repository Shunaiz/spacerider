﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyCamera : MonoBehaviour {

    public Transform player;

    public float zOffset;

    private float lerpValue= 1;

	
	// Update is called once per frame
	void Update ()
    {
        if (PauseMenu.IsPaused()) return;


        Vector3 newPosition = player.position;
        newPosition.z = player.transform.position.z - zOffset;
        transform.position = Vector3.Lerp(transform.position, newPosition, lerpValue);
        transform.LookAt(player);
	}
}
