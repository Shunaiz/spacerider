﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTrigger : MonoBehaviour
{
    public AudioClip SwitchHit;
    public AudioSource NoiseSource;
    public UnityEngine.Events.UnityEvent BossRoomTrigger;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bullet")
        {
            Debug.Log("Player shot trigger");
            NoiseSource.clip = SwitchHit;
            AudioSource.PlayClipAtPoint(NoiseSource.clip, transform.position);
            BossRoomTrigger.Invoke();
        }
        else if(other.tag == "Player")
        {
            NoiseSource.clip = SwitchHit;
            AudioSource.PlayClipAtPoint(NoiseSource.clip, transform.position);
            Debug.Log("Player hit trigger");
            BossRoomTrigger.Invoke();
        }
    }
}
