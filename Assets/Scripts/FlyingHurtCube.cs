﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingHurtCube : MonoBehaviour
{
    // Start is called before the first frame update
    public bool activated = false;
    private Vector3 angularVelocity;
    void Start()
    {
        angularVelocity = Random.insideUnitSphere * Random.Range(300, 800);
    }

    // Update is called once per frame
    void Update()
    {
        angularVelocity += Random.insideUnitSphere * Random.Range(-300, 300);
        transform.Rotate(angularVelocity * Time.deltaTime);

    }
}
