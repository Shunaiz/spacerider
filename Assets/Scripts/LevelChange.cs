﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class LevelChange : MonoBehaviour
{
   
   
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player")
        {

            GameObject player = GameObject.FindGameObjectWithTag("Player");
            player.GetComponent<Health_O2_Music>().SavePlayer();
            player.GetComponent<PlayerController>().SaveGun();
            SceneManager.LoadScene(3);

        }
    }

   
}
