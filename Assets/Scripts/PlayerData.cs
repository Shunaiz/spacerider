﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public int money;
    public int lives;
    public int maxHealth;
    public int maxOxygen;

    public PlayerData(Health_O2_Music player)
    {
        money = player.GetComponent<Health_O2_Music>().MoneyCheck();
        lives = player.GetComponent<Health_O2_Music>().LivesCheck();
        maxHealth = player.GetComponent<Health_O2_Music>().MaxHealthCheck();
        maxOxygen = player.GetComponent<Health_O2_Music>().MaxOxygenCheck();
    }
}
