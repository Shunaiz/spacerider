﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactorFloatingText : MonoBehaviour
{
    public UnityEngine.Events.UnityEvent EscapeDoorTrigger;

    // Start is called before the first frame update
    private ReactorController reactor;
    void Start()
    {
        reactor = GetComponentInParent<ReactorController>();
    }

    // Update is called once per frame
    void Update()
    {
        Transform playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        float playerDistance = Vector3.Distance(playerTransform.position, transform.position);

        //DEBUG: Check if the player is close and activate
        if (Vector3.Distance(GameObject.FindGameObjectWithTag("Player").transform.position, transform.position) < 5 && Input.GetKeyDown(KeyCode.E))
        {
            reactor.ActivateCountdown();
            EscapeDoorTrigger.Invoke();
        }
        if(reactor.IsActivated())
        {
            GetComponent<TextMesh>().text = "ESCAPE!";
        }
        else
        {
            GetComponent<TextMesh>().text = "Press E";
        }

        Color c = GetComponent<TextMesh>().color;
        c.a = Mathf.Clamp(-playerDistance + 5, 0, 1);
        GetComponent<TextMesh>().color = c;
    }
}
