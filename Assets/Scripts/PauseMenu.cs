﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{

    private int buttonWidth;
    private int buttonHeight;
    private int origin_x;
    private int origin_y;
    public GUISkin skin;

    private static bool isPaused = false;
    public static bool IsPaused()
    {
        return isPaused;
    }

    // Use this for initialization
    void Start()
    {
        buttonWidth = 200;
        buttonHeight = 50;
        origin_x = Screen.width / 2 - buttonWidth / 2;
        origin_y = Screen.height / 2 - buttonHeight * 2;
    }

    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
            TogglePause();
    }

    void OnGUI()
    {
        GUI.skin = skin;// Resources.Load<GUISkin>("Materials and Textures/UI/UISkin");

        if(isPaused)
        {
            if (GUI.Button(new Rect(origin_x, origin_y, buttonWidth, buttonHeight), "Resume"))
            {
                TogglePause();
            }
            if (GUI.Button(new Rect(origin_x-110, origin_y + buttonHeight * 1 + 10, buttonWidth, buttonHeight), "Return to Home"))
            {
                TogglePause();
                SceneManager.LoadScene(1);
            }
            if (GUI.Button(new Rect(origin_x+110, origin_y + buttonHeight * 1 + 10, buttonWidth, buttonHeight), "Restart Level"))
            {
                TogglePause();
                int scene = SceneManager.GetActiveScene().buildIndex;
                SceneManager.LoadScene(scene, LoadSceneMode.Single);
            }
            if (GUI.Button(new Rect(origin_x, origin_y + buttonHeight * 2 + 20, buttonWidth, buttonHeight), "Exit Game"))
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
            }
        }
        
    }

    public static bool TogglePause()
    {
        if (System.Math.Abs(Time.timeScale) < Mathf.Epsilon)
        {
            Time.timeScale = 1.0f;
            isPaused = false;
        }
        else
        {
            Time.timeScale = 0.0f;
            isPaused = true;
        }
        return isPaused;
    }
}
