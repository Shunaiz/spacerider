﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatSpawner : MonoBehaviour
{
    public GameObject target;
    public int rotateSpeed = 90;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Example");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, rotateSpeed * Time.deltaTime, 0);//rotates object (x,y,z)
        //move accordingly to what object needs.
    }

    IEnumerator Example()
    {
        yield return new WaitForSecondsRealtime(.1f);
        Destroy(target);
    }
}
