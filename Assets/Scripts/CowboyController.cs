﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 ************** THIS SCRIPT IS OLD, USE Rigidbody based PlayerController script INSTEAD ************************
     */

public class CowboyController : MonoBehaviour
{
    public float speed;
    public float gravity;
    public float jumpHeight;
    public float jumpHeight1;
    public float fallMultiplier = 2.5f;
    public float lowJumpModifier = 2f;

    public LayerMask ground;
    public Transform foot1;
    public Transform foot2;
    //public AudioSource audio;
    public Transform bulletPrefab;
    private GameObject firePosition;
    private Rigidbody rb;

    private Vector3 direction;
    private Vector3 walkingVelocity;
    private Vector3 fallingVelocity;
    private CharacterController controller;
    public static bool faceRight = true;

    private Animator anim;
    private int a_move_hash = Animator.StringToHash("speed");
    private int a_jump_hash = Animator.StringToHash("jump");
    private int a_crouch_hash = Animator.StringToHash("crouch");
    private int a_uncrouch_hash = Animator.StringToHash("uncrouch");

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        speed = 5.0f;
        gravity = 9.8f;
        jumpHeight = 16.0f;
        jumpHeight1 = 16.0f;
        direction = Vector3.zero;
        fallingVelocity = Vector3.zero;
        controller = GetComponent<CharacterController>();
        //audio = GetComponent<AudioSource>();

        anim = GetComponent<Animator>();
        firePosition = GameObject.FindGameObjectWithTag("firepos");
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseMenu.IsPaused()) return;

        direction.x = Input.GetAxis("Horizontal");
        direction.z = 0f;
        direction = direction.normalized;
        walkingVelocity = direction * speed;
        controller.Move(walkingVelocity * Time.deltaTime);

        if (direction != Vector3.zero)
        {
            transform.forward = direction;
            anim.SetFloat(a_move_hash, speed);

            if (direction.x > 0)
                faceRight = true;
            else
                faceRight = false;
            //Debug.Log(direction);
            //Debug.Log(move);
        }
        else { anim.SetFloat(a_move_hash, -1.0f); }

        bool isGrounded = Physics.CheckSphere(foot1.position, 0.2f, ground, QueryTriggerInteraction.Ignore);
        if (isGrounded)
        {
            fallingVelocity.y = 0f;
        }
        else
        {
            if(rb.velocity.y < 0) //if falling, speed up the fall
            {
                fallingVelocity.y -= gravity * (fallMultiplier - 1) * Time.deltaTime;
            }
            else if(rb.velocity.y > 0 && !Input.GetKey(KeyCode.Space)) //if jump is released early
            {
                fallingVelocity.y -= gravity * (lowJumpModifier - 1) * Time.deltaTime;
            }
            else //else travelling upwards in the jump
            {
                fallingVelocity.y -= gravity * Time.deltaTime;
            }
            
        }
            
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            //audio.Play();
            fallingVelocity.y = Mathf.Sqrt(gravity * jumpHeight);
            //Debug.Log("Jumped");
            anim.SetTrigger(a_jump_hash);
        }

        bool isGrounded1 = Physics.CheckSphere(foot2.position, 0.2f, ground, QueryTriggerInteraction.Ignore);
        if (isGrounded1)
        {
            fallingVelocity.y = 0f;
        }
        else
        {
            if (rb.velocity.y < 0) //if falling, speed up the fall
            {
                fallingVelocity.y -= gravity * (fallMultiplier - 1) * Time.deltaTime;
            }
            else if (rb.velocity.y > 0 && !Input.GetKey(KeyCode.Space)) //if jump is released early
            {
                fallingVelocity.y -= gravity * (lowJumpModifier - 1) * Time.deltaTime;
            }
            else //else travelling upwards in the jump
            {
                fallingVelocity.y -= gravity * Time.deltaTime;
            }

        }

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded1)
        {
            //audio.Play();
            fallingVelocity.y = Mathf.Sqrt(gravity * jumpHeight1);
            //Debug.Log("Jumped");
            anim.SetTrigger(a_jump_hash);
        }

        controller.Move(fallingVelocity * Time.deltaTime);
        

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            
            FireProjectile();
        }

        if(Input.GetKeyDown(KeyCode.LeftControl))
        {
            //Debug.Log("Crouched");
            anim.SetTrigger(a_crouch_hash);
            firePosition.transform.position += new Vector3(0f, -0.5f, 0f);
        }
        else if(Input.GetKeyUp(KeyCode.LeftControl))
        {
            //Debug.Log("Uncrouched");
            anim.SetTrigger(a_uncrouch_hash);
            firePosition.transform.position += new Vector3(0f, 0.5f, 0f);
        }
    }


    void FireProjectile()
    {
        Vector3 spawnLocation = firePosition.transform.position;

        Instantiate(bulletPrefab, spawnLocation, Quaternion.Euler(0f, 0f, -90f));
    }
}
