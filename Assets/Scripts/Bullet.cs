﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [Range(1, 40)]
    public float speed;

    [Range(1, 20)]
    public int damage;

    Vector3 direction;
    public AudioClip hitMarker;
    public AudioClip wallImpact;
    public AudioSource MusicSource;
    public GameObject hitParticle;

    // Start is called before the first frame update
    void Start()
    {
        if (CowboyController.faceRight)
        {
            direction = Vector3.up;
        }
        else
        {
            direction = Vector3.down;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseMenu.IsPaused()) return;

        transform.Translate(direction * speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Bullet hit " + other.name);
        if(other.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
        }
        else if(other.CompareTag("BossShield"))
        {
            Debug.Log("Hit boss shield");
            Destroy(this.gameObject);
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("Bullet hit " + collision.gameObject.name);

        //do damage
        if (collision.gameObject.CompareTag("BossShield")) //no damage to boss shield
        {
            Debug.Log("Hit boss shield");
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("Hit enemy");
            MusicSource.clip = hitMarker;
            AudioSource.PlayClipAtPoint(hitMarker, transform.position);
            StartCoroutine("SpawnHitEffect");
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.CompareTag("wall"))
        {
            MusicSource.clip = wallImpact;
            AudioSource.PlayClipAtPoint(wallImpact, transform.position);
            StartCoroutine("SpawnHitEffect");
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.CompareTag("MovingPlatform"))
        {
            MusicSource.clip = wallImpact;
            AudioSource.PlayClipAtPoint(wallImpact, transform.position);
            StartCoroutine("SpawnHitEffect");
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.CompareTag("Destructable"))
        {
            MusicSource.clip = wallImpact;
            AudioSource.PlayClipAtPoint(wallImpact, transform.position);
            StartCoroutine("SpawnHitEffect");
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
    }

    IEnumerator SpawnHitEffect()
    {
        //Debug.Log("Spawning hit effect");
        Instantiate(hitParticle, transform.position, Quaternion.identity);
        yield return new WaitForSecondsRealtime(0.5f);
        //set them to detroy on after stopped
    }
}
