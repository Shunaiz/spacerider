﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndLevel : MonoBehaviour
{

    public AudioClip endLevel;
    public AudioSource MusicSource;
    private int buttonWidth;
    private int buttonHeight;
    private int origin_x;
    private int origin_y;
    public GUISkin skin;
    public GameObject reactor;
    public int flag=0;

    private static bool isPaused = false;
    //public RawImage YouWIN;

    // Start is called before the first frame update
    void Start()
    {
        buttonWidth = 200;
        buttonHeight = 50;
        origin_x = Screen.width / 2 - buttonWidth / 2;
        origin_y = Screen.height / 2 - buttonHeight * 2;
    }

    // Update is called once per frame
    void Update()
    {
         
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player")
        {
            Debug.Log("gfdggfdgdgdfgdfgdfgdgsdfgg");
            Invoke("StageClear", 8f);
            //YouWIN.enabled = true;
            MusicSource.clip = endLevel;
            AudioSource.PlayClipAtPoint(endLevel, transform.position);

            //Disable Reactor
            
            if (reactor.GetComponent<ReactorController>().IsActivated())
            {
                reactor.GetComponent<ReactorController>().PauseCountdown();
            }
            flag = 1;

            // StageClear();

        }
    }
    void OnGUI()
        {
            GUI.skin = skin;// Resources.Load<GUISkin>("Materials and Textures/UI/UISkin");
        if (flag == 1)
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            
            if (GUI.Button(new Rect(origin_x, origin_y, buttonWidth, buttonHeight), "Congratulation You Win"))
            {

            }
            if (GUI.Button(new Rect(origin_x, origin_y + buttonHeight * 2 + 20, buttonWidth, buttonHeight), "Click here to Exit Game"))
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
            }
        }
        }

    void StageClear()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<Health_O2_Music>().ContractComplete(10000); //enough for a life.
        player.GetComponent<Health_O2_Music>().SavePlayer();
        player.GetComponent<PlayerController>().SaveGun();
        SceneManager.LoadScene(0);
    }
}
