﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class lv1return : MonoBehaviour
{

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseMenu.IsPaused()) return;
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player")
        {
            Debug.Log("Player hit trigger");
            Invoke("StageClear", 5f);
            //YouWIN.enabled = true;

            StageClear();
        }
    }

    void StageClear()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<Health_O2_Music>().SavePlayer();
        player.GetComponent<PlayerController>().SaveGun();
        SceneManager.LoadScene(2);
    }
}
