﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DissolveScript : MonoBehaviour
{
    public Material dissolveMat;
    public float alphaFade = 0;
    public float alphaMax = 100;

    // Start is called before the first frame update
    void Start()
    {
        dissolveMat.SetFloat("_cutoff", alphaFade / alphaMax);
    }


    void Update()
    {
        if(alphaFade<alphaMax)
            alphaFade += 2f;
            dissolveMat.SetFloat("_cutoff", alphaFade / alphaMax);
    }


}
