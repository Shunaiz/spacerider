﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_NonRotate : MonoBehaviour
{
    private Quaternion rot;

    // Start is called before the first frame update
    void Awake()
    {
        rot = transform.rotation;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.rotation = rot;
    }
}
