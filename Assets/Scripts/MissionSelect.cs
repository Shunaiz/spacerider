﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MissionSelect : MonoBehaviour
{
    private int buttonWidth;
    private int buttonHeight;
    private int origin_x;
    private int origin_y;
    public GUISkin skin;

    private static bool isPaused = false;
    public static bool IsPaused()
    {
        return isPaused;
    }

    // Use this for initialization
    void Start()
    {
        buttonWidth = 200;
        buttonHeight = 25;
        origin_x = Screen.width / 2 - buttonWidth / 2;
        origin_y = Screen.height / 2 - buttonHeight * 2;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
            TogglePause();
    }

    void OnGUI()
    {
        GUI.skin = skin;// Resources.Load<GUISkin>("Materials and Textures/UI/UISkin");

        if (isPaused)
        {
            if (GUI.Button(new Rect(origin_x, origin_y - buttonHeight * 1 - 5, buttonWidth, buttonHeight), "Keep Exploring"))
            {
                TogglePause();
            }
            if (GUI.Button(new Rect(origin_x, origin_y, buttonWidth, buttonHeight), "Difficulty"))
            {
                
            }
            if (GUI.Button(new Rect(origin_x-110, origin_y + buttonHeight * 1 + 5, buttonWidth, buttonHeight), "Easy"))
            {
                GameObject player = GameObject.FindGameObjectWithTag("Player");
                player.GetComponent<Health_O2_Music>().SavePlayer();
                player.GetComponent<PlayerController>().SaveGun();
                TogglePause();
                SceneManager.LoadScene(2);
            }
            if (GUI.Button(new Rect(origin_x+110, origin_y + buttonHeight * 1 + 5, buttonWidth, buttonHeight), "Hard"))
            {
                GameObject player = GameObject.FindGameObjectWithTag("Player");
                player.GetComponent<Health_O2_Music>().SavePlayer();
                player.GetComponent<PlayerController>().SaveGun();
                TogglePause();
                SceneManager.LoadScene(3);
            }
            if (GUI.Button(new Rect(origin_x-110, origin_y + buttonHeight * 2 + 10, buttonWidth, buttonHeight), "Return to Menu"))
            {
                GameObject player = GameObject.FindGameObjectWithTag("Player");
                player.GetComponent<Health_O2_Music>().SavePlayer();
                player.GetComponent<PlayerController>().SaveGun();
                TogglePause();
                SceneManager.LoadScene(0);
            }
            if (GUI.Button(new Rect(origin_x+110, origin_y + buttonHeight * 2 + 10, buttonWidth, buttonHeight), "Exit Game"))
            {
                GameObject player = GameObject.FindGameObjectWithTag("Player");
                player.GetComponent<Health_O2_Music>().SavePlayer();
                player.GetComponent<PlayerController>().SaveGun();
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
            }
        }
    }

    public static bool TogglePause()
    {
        if (System.Math.Abs(Time.timeScale) < Mathf.Epsilon)
        {
            Time.timeScale = 1.0f;
            isPaused = false;
        }
        else
        {
            Time.timeScale = 0.0f;
            isPaused = true;
        }
        return isPaused;
    }
}
