﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    public float speed = 10.0f;
    public float LifeTime = 3.0f;
    public int damage = 10;
    public AudioClip wallImpact;
    public AudioSource MusicSource;

    // Start is called before the first frame update
    void Start()
    {
        //Destroy(gameObject, LifeTime);
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseMenu.IsPaused()) return;

        transform.position += transform.forward * speed * Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {
        //do damage
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(this.gameObject);
        }
        if (collision.gameObject.CompareTag("wall"))
        {
            MusicSource.clip = wallImpact;
            AudioSource.PlayClipAtPoint(wallImpact, transform.position);
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)
    {

        //Destroy(this.gameObject);
    }
}
