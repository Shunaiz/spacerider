﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum FSMState_FinalBoss
{
    None, Wait, Attack, ShootUp, Dead
}

public class SimpleFSM_FinalBoss : FSM
{
    FSMState_FinalBoss currentState;

    public AudioClip Death;
    public AudioSource MusicSource;
    public UnityEngine.Events.UnityEvent BossDeathTrigger;
    public GameObject deathParticle;
    public GameObject Bullet;
    public GameObject SpecialBullet;
    public Transform OverheadShotPoint;
    private Rigidbody rb;

    private float specialShot1ElapsedTime;
    private float specialShot1Timer = 4f;
    private float overheadShotElapsedTime;
    private float overheadShotTimer = 1.5f;

    private float curSpeed;
    private float curSpeedFast = 7.0f;
    private float curSpeedSlow = 3.5f;
    private float curRotSpeed;
    public int shotAngle_x;
    public int overHeadShotAngle_x = -90;

    private bool bDead;
    private bool bFaceRight; //THIS IS HARDCODED, PAY ATTENTION TO HOW THE BOSS IS PLACED FACING IN THE LEVEL AT START!!
    private int health;
    public int startingHealth;
    public Image healthBar;

    private Animator anim;
    private int a_move_hash = Animator.StringToHash("bossSpeed");
    private int a_dead_hash = Animator.StringToHash("bossDead");

    private int listIndex;

    protected override void Init()
    {
        currentState = FSMState_FinalBoss.Wait;
        anim = GetComponent<Animator>();
        elapsedTime = 0f;
        specialShot1ElapsedTime = 0f;
        overheadShotElapsedTime = 0f;
        shootRate = 0.9f;
        bDead = false;
        bFaceRight = false; //ENSURE BOSS IS FACING LEFT IN LEVEL AT START -- OR SWITCH THIS
        health = startingHealth;
        curSpeed = curSpeedSlow;
        curRotSpeed = 20.0f;
        listIndex = 0;

        rb = GetComponent<Rigidbody>();

        //Get the target enemy(Player)
        GameObject objPlayer = GameObject.FindGameObjectWithTag("Player");
        playerTransform = objPlayer.transform;

        if (!playerTransform)
            Debug.Log("Player doesn't exist.. Please add one with Tag named 'Player'");
        if (!deathParticle)
            Debug.LogWarning("No death particle prefab loaded, add one in inspector");

        bulletSpawnPoint = gameObject.transform.GetChild(0).transform;

        FindNextPoint();
    }

    protected override void FSMUpdate()
    {
        //Debug.DrawRay(transform.position, transform.forward);

        switch (currentState)
        {
            case FSMState_FinalBoss.Wait: UpdateWaitState(); break;
            case FSMState_FinalBoss.Attack: UpdateAttackState(); break;
            case FSMState_FinalBoss.ShootUp: UpdateShootUpState(); break;
            case FSMState_FinalBoss.Dead: UpdateDeadState(); break;
        }

        elapsedTime += Time.deltaTime;
        specialShot1ElapsedTime += Time.deltaTime;
        overheadShotElapsedTime += Time.deltaTime;

        if (health <= 0)
            currentState = FSMState_FinalBoss.Dead;
        else if (health <= (startingHealth/2))
            specialShot1Timer = 2.5f; //special shots happen much more often when under 1/2 hp
        
    }

    protected override void FSMFixedUpdate()
    {

    }

    void UpdateWaitState() // in wait state the boss moves back and forth in its area (originally did this to test its movement and just left it like that instead of having it just sit there)
    {
        Quaternion targetRotation = Quaternion.LookRotation(destinationPos - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * curRotSpeed);

        //Check the distance with player
        //When the distance is near, transition to attack state
        if (Vector3.Distance(transform.position, playerTransform.position) <= 25.0f)
        {
            //Debug.Log("Switched into attack state");
            currentState = FSMState_FinalBoss.Attack;
        }
        else if (Vector3.Distance(transform.position, destinationPos) <= 1.0f)
        {
            FindNextPoint();
        }

        MoveForward();
    }

    //This is the defaults state when the player is in the boss room
    void UpdateAttackState()
    {
        Quaternion targetRotation = Quaternion.LookRotation(destinationPos - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * curRotSpeed);

        
        if (Vector3.Distance(transform.position, destinationPos) <= 1.0f)
        {
            FindNextPoint();
        }

        //Shooting behaviours here
        ShootBullet();
        SpecialShot1();
        MoveForward();
    }

    //When the Player is in an overhead trigger area
    void UpdateShootUpState()
    {
        Quaternion targetRotation = Quaternion.LookRotation(destinationPos - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * curRotSpeed);


        if (Vector3.Distance(transform.position, destinationPos) <= 1.0f)
        {
            FindNextPoint();
        }

        ShootOverhead();
        MoveForward();
    }

    void UpdateDeadState()
    {
        if (!bDead)
        {
            bDead = true;
            BossDeathTrigger.Invoke();
            MusicSource.clip = Death;
            AudioSource.PlayClipAtPoint(Death, transform.position);
            StartCoroutine("SpawnDeathEffect"); //explosion
            anim.SetFloat(a_move_hash, -1); //stop moving animation
            anim.SetBool(a_dead_hash, true); //set death animation

            GetComponent<Collider>().isTrigger = true; //set collider to trigger so it doesn't block the player
            gameObject.tag = "DeadEnemy"; //change tag so the dead body doesn't hurt the player
            

        }
        //Destroy(this.gameObject);
    }

    IEnumerator SpawnDeathEffect()
    {
        Debug.Log("Spawning death effect");
        Instantiate(deathParticle, transform.position, Quaternion.identity);
        yield return new WaitForSecondsRealtime(1.2f);
        deathParticle.SetActive(false);

    }

    void MoveForward()
    {
        // Movement vector
        Vector3 movement = new Vector3(transform.forward.x * curSpeed * Time.deltaTime, 0f, 0f);

        // Current position
        Vector3 currPosition = transform.position;

        // New position
        Vector3 newPosition = currPosition + movement;

        // Move the rigid body
        rb.MovePosition(newPosition);

        anim.SetFloat(a_move_hash, curSpeed);

        //Check which way the boss is walking
        if (transform.forward.x > 0)
            bFaceRight = true;
        else
            bFaceRight = false;
    }

    protected void FindNextPoint()
    {
        //Debug.Log("Finding next patrol point");

        int nextIndex = listIndex++ % pointList.Length;

        destinationPos = pointList[nextIndex].transform.position;
    }

    protected bool IsInCurrentRange(Vector3 pos)
    {
        float xPos = Mathf.Abs(pos.x - transform.position.x);

        if (xPos <= 4)
            return true;

        return false;
    }

    protected void ShootBullet() //standard shot that shoots 3 bullets in a fan
    {
        if (elapsedTime >= shootRate)
        {
            //Shoot the bullets
            if(bFaceRight)
            {
                Instantiate(Bullet, bulletSpawnPoint.position, bulletSpawnPoint.rotation); //straight shot
                Instantiate(Bullet, bulletSpawnPoint.position, Quaternion.Euler(shotAngle_x, 90, 0)); //angle down shot
                Instantiate(Bullet, bulletSpawnPoint.position, Quaternion.Euler(-shotAngle_x, 90, 0)); //angle up shot
            }
            else //facing left
            {
                Instantiate(Bullet, bulletSpawnPoint.position, bulletSpawnPoint.rotation); //straight shot
                Instantiate(Bullet, bulletSpawnPoint.position, Quaternion.Euler(shotAngle_x, -90, 0)); //angle down shot
                Instantiate(Bullet, bulletSpawnPoint.position, Quaternion.Euler(-shotAngle_x, -90, 0)); //angle up shot
            }
                
            elapsedTime = 0.0f;
        }
    }

    protected void SpecialShot1() //special shot that shoots 7 bullets in a fan from the front of the boss
    {
        if(specialShot1ElapsedTime >= specialShot1Timer)
        {
            //Do special shot
            if (bFaceRight)
            {
                Instantiate(SpecialBullet, bulletSpawnPoint.position, bulletSpawnPoint.rotation); //straight shot
                Instantiate(SpecialBullet, bulletSpawnPoint.position, Quaternion.Euler(shotAngle_x, 90, 0)); //angle down shot
                Instantiate(SpecialBullet, bulletSpawnPoint.position, Quaternion.Euler(shotAngle_x + 5, 90, 0));
                Instantiate(SpecialBullet, bulletSpawnPoint.position, Quaternion.Euler(shotAngle_x + 10, 90, 0));
                Instantiate(SpecialBullet, bulletSpawnPoint.position, Quaternion.Euler(-shotAngle_x, 90, 0)); //angle up shot
                Instantiate(SpecialBullet, bulletSpawnPoint.position, Quaternion.Euler(-shotAngle_x - 5, 90, 0));
                Instantiate(SpecialBullet, bulletSpawnPoint.position, Quaternion.Euler(-shotAngle_x - 10, 90, 0));
            }
            else //facing left
            {
                Instantiate(SpecialBullet, bulletSpawnPoint.position, bulletSpawnPoint.rotation); //straight shot
                Instantiate(SpecialBullet, bulletSpawnPoint.position, Quaternion.Euler(shotAngle_x, -90, 0)); //angle down shot
                Instantiate(SpecialBullet, bulletSpawnPoint.position, Quaternion.Euler(shotAngle_x + 5, -90, 0));
                Instantiate(SpecialBullet, bulletSpawnPoint.position, Quaternion.Euler(shotAngle_x + 10, -90, 0));
                Instantiate(SpecialBullet, bulletSpawnPoint.position, Quaternion.Euler(-shotAngle_x, -90, 0)); //angle up shot
                Instantiate(SpecialBullet, bulletSpawnPoint.position, Quaternion.Euler(-shotAngle_x - 5, -90, 0));
                Instantiate(SpecialBullet, bulletSpawnPoint.position, Quaternion.Euler(-shotAngle_x - 10, -90, 0));
            }

            specialShot1ElapsedTime = 0;
        }
    }

    protected void ShootOverhead()
    {
        if(overheadShotElapsedTime >= overheadShotTimer)
        {
            //Instantiate(SpecialBullet, OverheadShotPoint.position, OverheadShotPoint.rotation); //need to rotate the shot to face up
            Instantiate(SpecialBullet, OverheadShotPoint.position, Quaternion.Euler(overHeadShotAngle_x, -90, 0));
            Instantiate(SpecialBullet, OverheadShotPoint.position, Quaternion.Euler(overHeadShotAngle_x - 15, -90, 0));
            Instantiate(SpecialBullet, OverheadShotPoint.position, Quaternion.Euler(overHeadShotAngle_x + 15, -90, 0));
            Instantiate(SpecialBullet, OverheadShotPoint.position, Quaternion.Euler(overHeadShotAngle_x - 25, -90, 0));
            Instantiate(SpecialBullet, OverheadShotPoint.position, Quaternion.Euler(overHeadShotAngle_x + 25, -90, 0));

            overheadShotElapsedTime = 0f;
        }


    }

    //Will be called when the player enters an overhead trigger area above the boss
    public void ShootOverheadEnter() 
    {
        if(!bDead)
        {
            curSpeed = curSpeedSlow;
            currentState = FSMState_FinalBoss.ShootUp;
        }
    }

    public void ShootOverheadExit()
    {
        if(!bDead)
        {
            curSpeed = curSpeedFast;
            currentState = FSMState_FinalBoss.Attack;
        }
    }


    private void OnTriggerEnter(Collider other)
    {

        //Reduce health
        if (other.tag == "Bullet")
        {
            //bullet does nothing if it hits the boss shield trigger (for now)
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        //Reduce health
        if (collision.gameObject.tag == "Bullet")
        {
            health -= collision.gameObject.GetComponent<Bullet>().damage;
            healthBar.fillAmount = ((float)health / (float)startingHealth);
            Debug.Log("Took " + collision.gameObject.GetComponent<Bullet>().damage + " damage");
        }
    }
}
