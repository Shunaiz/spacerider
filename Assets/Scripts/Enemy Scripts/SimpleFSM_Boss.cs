﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum FSMState_Boss
{
    None, Patrol, Attack, Shoot, Dead
}

public class SimpleFSM_Boss : FSM
{
    FSMState_Boss currentState;

    public AudioClip Death;
    public AudioSource MusicSource;
    public UnityEngine.Events.UnityEvent BossDeathTrigger;
    public GameObject deathParticle;

    public GameObject Bullet;

    private float curSpeed;
    private float curRotSpeed;
    private bool bDead;
    private int health;
    public int startingHealth;
    public Image healthBar;

    protected override void Init()
    {
        currentState = FSMState_Boss.Patrol;

        elapsedTime = 0f;
        shootRate = 1.0f;
        bDead = false;
        startingHealth = 30;
        health = startingHealth;
        curSpeed = 3.0f;
        curRotSpeed = 20.0f;
        pointList = GameObject.FindGameObjectsWithTag("BossMovePoint");

        //Set Random destination point first
        FindNextPoint();

        //Get the target enemy(Player)
        GameObject objPlayer = GameObject.FindGameObjectWithTag("Player");
        playerTransform = objPlayer.transform;

        if (!playerTransform)
            Debug.Log("Player doesn't exist.. Please add one with Tag named 'Player'");
        if (!deathParticle)
            Debug.LogWarning("No death particle prefab loaded, add one in inspector");

        bulletSpawnPoint = gameObject.transform.GetChild(0).transform;
    }

    protected override void FSMUpdate()
    {
        Debug.DrawRay(bulletSpawnPoint.transform.position, bulletSpawnPoint.forward);

        switch (currentState)
        {
            case FSMState_Boss.Patrol: UpdatePatrolState(); break;
            case FSMState_Boss.Attack: UpdateAttackState(); break;
            case FSMState_Boss.Shoot: UpdateShootState(); break;
            case FSMState_Boss.Dead: UpdateDeadState(); break;
        }

        elapsedTime += Time.deltaTime;

        if (health <= 0)
            currentState = FSMState_Boss.Dead;
    }

    protected override void FSMFixedUpdate()
    {

    }

    void UpdatePatrolState()
    {
        //Find another random patrol point if the current point is reached
        if (Vector3.Distance(transform.position, destinationPos) <= 1.0f)
        {
            //Debug.Log("Reached to the destination point\ncalculating the next point");
            FindNextPoint();
        }
        //Check the distance with player
        //When the distance is near, transition to attack state
        else if (Vector3.Distance(transform.position, playerTransform.position) <= 15.0f)
        {
            //Debug.Log("Switched into attack state");
            currentState = FSMState_Boss.Attack;
        }

        //Rotate to the target point
        Quaternion targetRotation = Quaternion.LookRotation(destinationPos - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * curRotSpeed);

        //Go Forward
        transform.Translate(Vector3.forward * Time.deltaTime * curSpeed);
    }

    void UpdateAttackState()
    {
        //Set the target position as the player position
        destinationPos = playerTransform.position + new Vector3(0f, .5f, 0f);
        Quaternion targetRotation = Quaternion.LookRotation(destinationPos - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * curRotSpeed);

        //Check the distance with player
        //When the distance is near, transition to shoot state
        float dist = Vector3.Distance(transform.position, playerTransform.position);
        if (dist <= 9.0f)
        {
            //Debug.Log("Switched into shoot state");
            currentState = FSMState_Boss.Shoot;
        }
        //Go back to patrol is it become too far
        else if (dist > 15.0f)
        {
            FindNextPoint();
            //Debug.Log("Switched into patrol state");
            currentState = FSMState_Boss.Patrol;
            FindNextPoint();
        }

        //Go Forward
        transform.Translate(Vector3.forward * Time.deltaTime * curSpeed);
    }

    void UpdateShootState()
    {
        //Set the target position as the player position
        destinationPos = playerTransform.position + new Vector3(0f, 1f, 0f);

        //Check the distance with the player 
        float dist = Vector3.Distance(transform.position, playerTransform.position);
        if (dist >= 3.0f && dist < 9.0f)
        {
            //Rotate to the target point
            Quaternion targetRotation = Quaternion.LookRotation(destinationPos - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * curRotSpeed);

            //Go Forward
            transform.Translate(Vector3.forward * Time.deltaTime * curSpeed);

            currentState = FSMState_Boss.Shoot;
        }
        else if (dist > 9.0f && dist < 15.0f)
        {
            //Debug.Log("Switched into attack state");
            currentState = FSMState_Boss.Attack;
        }
        //Transition to patrol is the player becomes too far
        else if (dist > 15.0f)
        {
            //Debug.Log("Switched into patrol state");
            currentState = FSMState_Boss.Patrol;
            FindNextPoint();
        }

        //Shoot the bullets
        ShootBullet();

    }

    void UpdateDeadState()
    {
        if (!bDead)
        {
            bDead = true;
            BossDeathTrigger.Invoke();
            MusicSource.clip = Death;
            AudioSource.PlayClipAtPoint(Death, transform.position);
            StartCoroutine("SpawnDeathEffect");
        }
        Destroy(this.gameObject);
    }

    IEnumerator SpawnDeathEffect()
    {
        Instantiate(deathParticle, transform.position, Quaternion.identity);
        yield return new WaitForSecondsRealtime(1.2f);
        deathParticle.SetActive(false);

    }

    protected void FindNextPoint()
    {
        //Debug.Log("Finding next patrol point");

        int randIndex = Random.Range(0, pointList.Length);
        float rndRadius = 2.0f;

        Vector3 rndPosition = Vector3.zero;
        destinationPos = pointList[randIndex].transform.position + rndPosition;

        if (IsInCurrentRange(destinationPos))
        {
            rndPosition = new Vector3(Random.Range(-rndRadius, rndRadius), 0.0f, 0.0f);
            destinationPos = pointList[randIndex].transform.position + rndPosition;
        }
    }

    protected bool IsInCurrentRange(Vector3 pos)
    {
        float xPos = Mathf.Abs(pos.x - transform.position.x);

        if (xPos <= 4)
            return true;

        return false;
    }

    protected void ShootBullet()
    {
        if (elapsedTime >= shootRate)
        {
            //Shoot the bullet
            Instantiate(Bullet, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
            elapsedTime = 0.0f;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        
        //Reduce health
        if (other.tag == "Bullet")
        {
            Debug.Log("Boss hit by bullet");
            health -= other.gameObject.GetComponent<Bullet>().damage;
            healthBar.fillAmount = ((float)health / (float)startingHealth);
        }
    }
}
