﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum FSMState_ExploderTrig
{
    None, Sit, Attack, Reset, Explode, Dead
}


public class SimpleFSM_ExploderTrig : FSM
{
    public AudioClip Death;
    public AudioSource MusicSource;
    private Rigidbody rb;
    public GameObject deathParticle;

    FSMState_ExploderTrig currentState;
    private Vector3 startPos;

    [Range(1, 10)]
    public float curSpeed;

    [Range(1, 40)]
    public int explosion_damage;

    [Range(1, 100)]
    public int startingHealth;

    private float curRotSpeed;
    private bool bDead;
    public int health;
    public Image healthBar;
    private int patrolIndex;
    private int numPatrolPoints;

    private float y_clamp; //keeps enemy from flying toward player
    private float left_x_clamp; //bounds movement to the left
    private float right_x_clamp; //bounds movement to the right

    protected override void Init()
    {
        currentState = FSMState_ExploderTrig.Sit;
        rb = GetComponent<Rigidbody>();
        startPos = transform.position;
        elapsedTime = 0f;
        shootRate = 1.5f; //inherited from FSM, but not used
        bDead = false;
        health = startingHealth;
        curRotSpeed = 20.0f;
        patrolIndex = 0;
        numPatrolPoints = pointList.Length;
        y_clamp = transform.position.y;

        if (pointList == null)
            Debug.LogError("No bounding points for exploder... Please add 2 to this enemy in the inspector");
        else
        {
            if (pointList[0].transform.position.x > pointList[1].transform.position.x)
            {
                right_x_clamp = pointList[0].transform.position.x;
                left_x_clamp = pointList[1].transform.position.x;
            }
            else
            {
                right_x_clamp = pointList[1].transform.position.x;
                left_x_clamp = pointList[0].transform.position.x;
            }
        }

        //Get the target enemy(Player)
        GameObject objPlayer = GameObject.FindGameObjectWithTag("Player");
        playerTransform = objPlayer.transform;

        if (!playerTransform)
            Debug.Log("Player doesn't exist.. Please add one with Tag named 'Player'");
        if (!deathParticle)
            Debug.LogWarning("No death particle prefab loaded, add one in inspector");

    }

    protected override void FSMUpdate()
    {

        switch (currentState)
        {
            case FSMState_ExploderTrig.Sit: UpdateSitState(); break;
            case FSMState_ExploderTrig.Attack: UpdateAttackState(); break;
            case FSMState_ExploderTrig.Reset: UpdateResetState(); break;
            case FSMState_ExploderTrig.Explode: UpdateExplodeState(); break;
            case FSMState_ExploderTrig.Dead: UpdateDeadState(); break;
        }

        elapsedTime += Time.deltaTime;

        if (health <= 0)
            currentState = FSMState_ExploderTrig.Dead;
    }

    protected override void FSMFixedUpdate()
    {

    }

    void UpdateSitState()
    {
        //Check the distance with player
        //When the distance is near, transition to attack state
        if (Vector3.Distance(transform.position, playerTransform.position) <= 4.0f)
        {
            //Debug.Log("Switched into attack state");
            currentState = FSMState_ExploderTrig.Attack;
        }
    }

    void UpdateAttackState()
    {
        //Set the target position as the player position
        destinationPos = new Vector3(playerTransform.position.x, y_clamp, playerTransform.position.z);
        Quaternion targetRotation = Quaternion.LookRotation(destinationPos - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * curRotSpeed);

        //Check the distance with player
        //When the distance is near, transition to explode state
        float dist = Vector3.Distance(transform.position, playerTransform.position);
        if (dist <= 1.0f)
        {
            //Debug.Log("Switched into explode state");
            currentState = FSMState_ExploderTrig.Explode;
        }

        //Go Forward
        transform.Translate(Vector3.forward * Time.deltaTime * curSpeed);

        //if the player leaves the area the exploder patrols, reset it
        if (transform.position.x > right_x_clamp || transform.position.x < left_x_clamp)
        {
            currentState = FSMState_ExploderTrig.Reset;
        }
    }

    void UpdateResetState()
    {
        //Set the target position as the starting position
        destinationPos = new Vector3(startPos.x, y_clamp, 0f);
        Quaternion targetRotation = Quaternion.LookRotation(destinationPos - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * curRotSpeed);

        //Check the distance with destination
        //When the distance is near, transition to patrol state
        float dist = Vector3.Distance(transform.position, destinationPos);
        if (dist <= 1.0f)
        {
            //Debug.Log("Switched into sit state");
            currentState = FSMState_ExploderTrig.Sit;
        }

        //Go Forward
        transform.Translate(Vector3.forward * Time.deltaTime * curSpeed);
    }

    void UpdateExplodeState()
    {
        //Explode on the player
        Explode();

        //enter dead state
        currentState = FSMState_ExploderTrig.Dead;
    }

    void UpdateDeadState()
    {
        if (!bDead)
        {
            bDead = true;
            MusicSource.clip = Death;
            AudioSource.PlayClipAtPoint(Death, transform.position);
            StartCoroutine("SpawnDeathEffect");
        }
        Destroy(this.gameObject);
    }

    IEnumerator SpawnDeathEffect()
    {
        Instantiate(deathParticle, transform.position, Quaternion.identity);
        yield return new WaitForSecondsRealtime(1.1f);
        //deathParticle.SetActive(false);

    }

    void Explode()
    {
        //try setting an explosive force?
        rb.AddExplosionForce(100.0f, transform.position, 1.0f);


        //do damage to the player
        playerTransform.GetComponent<Health_O2_Music>().TakeExplosion(explosion_damage);

    }


    protected bool IsInCurrentRange(Vector3 pos)
    {
        float xPos = Mathf.Abs(pos.x - transform.position.x);

        if (xPos <= 4)
            return true;

        return false;
    }

    void OnCollisionEnter(Collision collision)
    {
        //Reduce health
        if (collision.gameObject.tag == "Bullet")
        {
            health -= collision.gameObject.GetComponent<Bullet>().damage;
            healthBar.fillAmount = ((float)health / (float)startingHealth);

            currentState = FSMState_ExploderTrig.Attack; //even if player is out of sensing range, if it gets hit with a bullet, go to attack state
        }
        else if(collision.gameObject.tag == "Player")
        {
            currentState = FSMState_ExploderTrig.Explode;
        }
    }

    public void enterAttackStateTrigger()
    {
        currentState = FSMState_ExploderTrig.Attack;
    }

    public void enterResetStateTrigger()
    {
        currentState = FSMState_ExploderTrig.Reset;
    }
}
