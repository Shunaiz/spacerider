﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyTrigger : MonoBehaviour
{
    public UnityEngine.Events.UnityEvent SpawnEnemiesTrigger;

    public AudioClip Siren;
    public AudioSource MusicSource;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseMenu.IsPaused()) return;
    }

    private void OnTriggerEnter(Collider other)
    {

        if(other.tag == "Player")
        {
            Debug.Log("Player hit trigger");
            SpawnEnemiesTrigger.Invoke();
            MusicSource.clip = Siren;
            AudioSource.PlayClipAtPoint(Siren, transform.position);
        }
            
    }
}
