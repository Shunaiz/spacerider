﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM : MonoBehaviour {

    protected Transform playerTransform;
    protected Vector3 destinationPos;
    public GameObject[] pointList;
    protected float elapsedTime;
    protected float shootRate;

    public Transform bulletSpawnPoint { get; set; }

    protected virtual void Init() { }
    protected virtual void FSMUpdate() { }
    protected virtual void FSMFixedUpdate() { }


	// Use this for initialization
	void Start ()
    {
        Init();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (PauseMenu.IsPaused()) return;

        FSMUpdate();
	}

    void FixedUpdate()
    {
        FSMFixedUpdate();
    }
}
