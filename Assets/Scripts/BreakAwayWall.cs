﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakAwayWall : MonoBehaviour
{
    public GameObject breakAwayParent;

    // Start is called before the first frame update

    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            foreach (Transform child in breakAwayParent.transform)
                child.gameObject.AddComponent<FlyAway>();
        }
    }
}