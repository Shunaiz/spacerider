﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyAway : MonoBehaviour
{
    private Vector3 velocity;
    private Vector3 angularVelocity;

    // Start is called before the first frame update
    void Start()
    {
        velocity = Random.insideUnitSphere * Random.Range(1, 20);
        angularVelocity = Random.insideUnitSphere * Random.Range(100, 300);
        Invoke("Delete", 5f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += velocity * Time.deltaTime;
        transform.Rotate(angularVelocity * Time.deltaTime);
    }

    void Delete()
    {
        Destroy(gameObject);
    }
}
