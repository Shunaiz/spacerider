﻿using UnityEngine;
using System;
using System.Collections.Generic;

[Serializable]
public class CharacterData {
    public int spaceCash = 0;
    public int maxHP = 100;
    public int maxO2 = 100;

    //Owned Weapons on Spaceship
    public enum SpaceShipWeapons { PEASHOOTER };
    public Dictionary<SpaceShipWeapons, bool> spaceshipWeapons;

    //Owned Weapons on Platformer
    public enum PlatformerWeapons { PEASHOOTER };
    public Dictionary<PlatformerWeapons, bool> platformerWeapons;
    //-1 means unlimited ammo
    public Dictionary<PlatformerWeapons, int> platformerWeaponsAmmo;
}

public class PlayerPrefsCharacterSaver : MonoBehaviour
{
    public static CharacterData characterData;

    private void Start()
    {
        characterData = LoadCharacter();

        //Give default weapons
        characterData.spaceshipWeapons[CharacterData.SpaceShipWeapons.PEASHOOTER] = true;

        characterData.platformerWeapons[CharacterData.PlatformerWeapons.PEASHOOTER] = true;
        characterData.platformerWeaponsAmmo[CharacterData.PlatformerWeapons.PEASHOOTER] = -1;
    }

    void Update()
    {
    }

    static void SaveCharacter(CharacterData data)
    {
        string jsonString = JsonUtility.ToJson(data);
        PlayerPrefs.SetString("savegame", jsonString);
        PlayerPrefs.Save();
    }

    static CharacterData LoadCharacter()
    {
        if(PlayerPrefs.HasKey("savegame"))
            return JsonUtility.FromJson<CharacterData>(PlayerPrefs.GetString("savegame"));
        return new CharacterData();
    }
}