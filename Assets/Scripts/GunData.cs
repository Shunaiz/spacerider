﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GunData
{
    public bool threeRoundMat;
    public bool shotgunMat;
    public bool heavyMat;
    public bool fullAutoMat;
    public bool rocketMat;

    public GunData(PlayerController gun)
    {
        threeRoundMat = gun.GetComponent<PlayerController>().ThreeRoundMatCheck();
        shotgunMat = gun.GetComponent<PlayerController>().ShotgunMatCheck();
        heavyMat = gun.GetComponent<PlayerController>().HeavyMatCheck();
        fullAutoMat = gun.GetComponent<PlayerController>().FullAutoMatCheck();
        rocketMat = gun.GetComponent<PlayerController>().RocketMatCheck();
    }
}
