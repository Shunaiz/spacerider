﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReactorController : MonoBehaviour
{
    public float countdownTime = 30.0f;
    public float sirenStartTime = 10.0f;
    public Text ReactorText;
    public GameObject groundParent;
    public GameObject[] endBlocks;
    public GameObject explosionPrefab;

    private float timer;
    private bool activated = false;

    // Start is called before the first frame update
    void Start()
    {
        ResetCountdown();
    }

    public void ActivateCountdown()
    {
        activated = true;
        foreach (var item in endBlocks)
        {
            item.active = false;
        }
    }

    public void PauseCountdown()
    {
        GetComponent<AudioSource>().Stop();
        activated = false;
    }

    public void ResetCountdown()
    {
        PauseCountdown();
        timer = countdownTime;
        foreach (var item in endBlocks)
        {
            item.active = true;
        }
    }

    public bool IsActivated()
    {
        return activated;
    }

    public float GetTimeLeft()
    {
        return timer;
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseMenu.IsPaused()) return;

        //DEBUG: Check if the player is close and activate
        //edited out so the player can activate instead
        //if (Vector3.Distance(GameObject.FindGameObjectWithTag("Player").transform.position, transform.position) < 2)
            //ActivateCountdown();

        //Make the player controller Update Text if activated
        if (IsActivated())
        {
            ReactorText.text = "Reactor: " + GetTimeLeft().ToString("0.00");
        } else
        {
            ReactorText.text = "";
        }

        if (activated)
            timer -= Time.deltaTime;
        
        if(timer <= 0)
        {
            timer = 0;
            PauseCountdown();
            ReactorText.text = "";

            //Set HP to 0
            GameObject.FindGameObjectWithTag("Player").GetComponent<Health_O2_Music>().Kill();

            //Fly away all objects
            foreach (Transform child in groundParent.transform)
                child.gameObject.AddComponent<FlyAway>();

            //TODO: Explosion Particles
            Instantiate(explosionPrefab);

            Destroy(gameObject);
        } else if(timer <= sirenStartTime)
        {
            //Alert to player 10 seconds remaining
            if(!GetComponent<AudioSource>().isPlaying)
                GetComponent<AudioSource>().Play();
        }
    }
}
