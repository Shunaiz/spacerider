﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TipsToggle : MonoBehaviour
{
    private static bool tips = true;
    public GameObject gameTips;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            ToggleTips();
        }
    }

    public void ToggleTips()
    {
        if (tips == true)
        {
            gameTips.SetActive(false);
            tips = false;
        }
        else
        {
            gameTips.SetActive(true);
            tips = true;
        }
    }
    
}
