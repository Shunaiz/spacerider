﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    //for xyz mouse coordinates
    public Camera cam;
    //can use this for our charcter lasr weapon
    public GameObject firepoint;
    //need the reference of lazer.
    public LineRenderer lr;
    //of the laser
    public float maxLength;

    void Update()
    {
        lr.SetPosition(0, firepoint.transform.position);

        RaycastHit hit;
        var mousePos = Input.mousePosition;
        var rayMouse = cam.ScreenPointToRay(mousePos);

        if(Physics.Raycast (rayMouse.origin, rayMouse.direction, out hit, maxLength))
        {
            if(hit.collider)
            {
                lr.SetPosition(1, hit.point);
            }
            else
            {
                var pos = rayMouse.GetPoint(maxLength);
                lr.SetPosition(1, pos);
            }
        }

    }
}