﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{

    public Text nameText;
    public Animator animator;
    public Text dialogueText;

    private Queue<string> sentences;

    private static bool isPaused = false;
    public static bool IsPaused()
    {
        return isPaused;
    }

    // Start is called before the first frame update
    void Start()
    {
        sentences = new Queue<string>();
    }

    private void Update()
    {
        if (PauseMenu.IsPaused()) return;
    }

    public void StartDialogue(Dialogue dialogue)
    {

        animator.SetBool("IsOpen", true);

        nameText.text = dialogue.name;

        sentences.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();

        StartCoroutine(Example());

    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));

    }


    IEnumerator TypeSentence(string sentence)
    {
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
    }

    IEnumerator Example()
    {
        print(Time.time);
        yield return new WaitForSeconds(.3f);
        TogglePause();
        print(Time.time);
    }

    void EndDialogue()
    {
        animator.SetBool("IsOpen", false);
        TogglePause();

    }

    public static bool TogglePause()
    {
        if (System.Math.Abs(Time.timeScale) < Mathf.Epsilon)
        {
            Time.timeScale = 1.0f;
            isPaused = false;
        }
        else
        {
            Time.timeScale = 0.0f;
            isPaused = true;
        }
        return isPaused;
    }

}
