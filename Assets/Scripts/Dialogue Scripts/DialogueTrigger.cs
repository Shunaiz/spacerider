﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{

    public Dialogue dialogue;

    public void Update()
    {
        Transform playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        float playerDistance = Vector3.Distance(playerTransform.position, transform.position);

        //DEBUG: Check if the player is close and activate
        if (Vector3.Distance(GameObject.FindGameObjectWithTag("Player").transform.position, transform.position) < 5 && Input.GetKeyDown(KeyCode.C))
        {
            TriggerDialogue();
            GetComponent<TextMesh>().text = "'C' to Chat";
        }

        Color c = GetComponent<TextMesh>().color;
        c.a = Mathf.Clamp(-playerDistance + 5, 0, 1);
        GetComponent<TextMesh>().color = c;

    }

    public void TriggerDialogue()
    {
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
    }

}
