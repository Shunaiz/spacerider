﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{

    public Transform spawnPoint;
    public AudioClip MusicClip;
    public AudioSource MusicSource;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            AudioSource.PlayClipAtPoint(MusicClip, transform.position);
            other.gameObject.transform.position = spawnPoint.position;
        }
    }
}
