﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Health_O2_Music : MonoBehaviour
{
    //public Text countText;          //Store a reference to the UI Text component which will display the number of pickups collected.
    //public Text winText;            //Store a reference to the UI Text component which will display the 'You win' message.
    public Text lifeText;           //Store a reference to the UI Text component which will display Life
    public Text OxygenText;         //stores the oxygen amounts and displays
    public Text MoneyText;          //stores the amount of money and displays
    public Text Lives;              //displays the ammpunt of lives player has.
    public RawImage dead;           //displays when dead (we can change to player icon)
    public RawImage maskOn;         //displays when o2 mask is on
    public RawImage miniMapBackground; //to undisplay
    public RawImage miniMap;   //to undisplay
    public Text maskState;          //stores the mask and displays
    public RawImage redFlash;  //image to make the screen flash red when the player gets hit

    public AudioClip MusicClip;
    public AudioClip PlayerHit;
    //public AudioClip StageMusic;
    public AudioSource MusicSource;
    public AudioSource PlayerSource;

    public ParticleSystem effect;

    private Rigidbody2D rb2d;       //Store a reference to the Rigidbody2D component required to use 2D Physics.
    public int countHP;            //counter for Health
    private int countOx;            //counter for Oxygen
    static private int countMoney = 0;//counter for Gold/Money
    private int tempMoney;
    static private int playerLives = 5;//# of lives that carry through each level
    public Image healthBar;         //yeah
    public Image oxygenBar;         //yeah
    private static int HealthMax = 100;   //set Max health for character hp bar
    private static int OxygenMax = 50;    //defaults for new game
    private string maskOnOff = "On";
    private static bool isPaused = false;
    private bool takeDamage;
    private bool tempGun = false; //for gunhandler
    private int tempGunNum = 0;
    private Animator anim;
    private int a_noair_hash = Animator.StringToHash("noair");
    private int a_dead_hash = Animator.StringToHash("dead");


    // Use this for initialization
    void Start()
    {
        //Initialize count to zero.
        countHP = HealthMax;
        countOx = OxygenMax;
        tempMoney = 0;
        dead.enabled = false;
        maskOn.enabled = true;
        miniMap.enabled = true;
        miniMapBackground.enabled = true;
        anim = GetComponent<Animator>();
        //Initialze winText to a blank string since we haven't won yet at beginning.
        //winText.text = "";
        //countText.text = "Collectables: " + count.ToString() + "/" + numtowin.ToString();
        lifeText.text = "HP: " + countHP.ToString() + "/" + HealthMax.ToString();
        OxygenText.text = "Oxygen: " + countOx.ToString() + "/" + OxygenMax.ToString();
        MoneyText.text = "Money: $" + countMoney.ToString();
        maskState.text = "Helmet: " + maskOnOff;
        Lives.text = "Lives: " + playerLives.ToString();
        //MusicSource.clip = StageMusic;
        //AudioSource.PlayClipAtPoint(StageMusic, transform.position);
        takeDamage = false;
        redFlash.enabled = true;

        //Call our SetCountText function which will update the text with the current value for count.
        SetCountText();
    }

    private void Update()
    {
        if (PauseMenu.IsPaused()) return;
        if (Input.GetKeyDown(KeyCode.H))
        {
            if (maskOn.enabled == true)
            {
                maskOn.enabled = false;
                maskOnOff = "OFF";
                SetCountText();
            }
            else
            {
                maskOn.enabled = true;
                maskOnOff = "ON";
                SetCountText();
            }

        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (miniMap.enabled == true)
            {
                miniMap.enabled = false;
                miniMapBackground.enabled = false;
            }
            else
            {
                miniMap.enabled = true;
                miniMapBackground.enabled = true;
            }
        }
        GameOver();
        ScreenFlashOnDamage();
    }

    //OnTriggerEnter2D is called whenever this object overlaps with a trigger collider.
    void OnTriggerEnter(Collider other)
    {
        //Debug.Log("OnTrigger in Health_o2");
        //Check the provided Collider2D parameter other to see if it is tagged "PickUp", if it is...
        //(Robert) Changed this part to medkit pickup
        if (other.gameObject.CompareTag("Medkit"))
        {
            if (countHP >= (HealthMax - 20) && countHP <= HealthMax)
            {
                //Debug.Log("grabbed a medkit for Full HP");
                //Add one to the current value of our count variable.
                countHP = HealthMax;
                healthBar.fillAmount = ((float)countHP / (float)HealthMax);
                //^^^adjusts health bar.
            }
            else if (countHP <= (HealthMax - 20))
            {
                //Debug.Log("grabbed a medkit for 20HP");
                //Add one to the current value of our count variable.
                countHP = countHP + 20;
                healthBar.fillAmount = ((float)countHP / (float)HealthMax);
                //^^^adjusts health bar.
            }
            else
            { }
        }

        if (other.gameObject.CompareTag("Air"))
        {
            if (maskOn.enabled == true)
            {
                InvokeRepeating("DecreaseOxygenNormal", 1.0f, 5f);
                //Debug.Log("No O2 with mask trigger");

                //^^^adjusts O2 bar.
            }
            if (maskOn.enabled == false) //restores naturally
            {
                InvokeRepeating("RecoverOxygen", 1.0f, 1f);
            }
        }

        if (other.gameObject.CompareTag("NoAir"))
        {
            if (maskOn.enabled == true)
            {
                InvokeRepeating("DecreaseOxygen", 1.0f, 1f);
                //Debug.Log("No O2 with mask trigger");

                //^^^adjusts O2 bar.
            }
            else
            {
                InvokeRepeating("DecreaseOxygen", 1.0f, 1f);
                //Debug.Log("No O2 with mask trigger");
                //^^^adjusts O2 bar.
            }
        }

        //Check the provided Collider2D parameter other to see if it is tagged "PickUp", if it is...
        //(Robert) Changed this part to Space Cash pickup
        if (other.gameObject.CompareTag("GoldBrick"))
        {
            //adds 500 for each brick of gold for now
            countMoney = countMoney + 500;
            tempMoney = tempMoney + 500;
        }

        if (other.gameObject.CompareTag("EnemyBullet"))
        {
            //Debug.Log("hit by enemy bullet in trigger");
            //less one to the current value of our hp count variable.
            countHP = countHP - other.gameObject.GetComponent<EnemyBullet>().damage;
            PlayerSource.clip = PlayerHit;
            AudioSource.PlayClipAtPoint(PlayerSource.clip, transform.position);
            healthBar.fillAmount = ((float)countHP / (float)HealthMax);
            takeDamage = true;

        }

        if (other.gameObject.CompareTag("TrbMat"))
        {
            this.GetComponent<PlayerController>().SetTrbAllow();
            tempGun = true;
            tempGunNum = 1;
        }
        if (other.gameObject.CompareTag("ShotgunMat"))
        {
            this.GetComponent<PlayerController>().SetShotgunAllow();
            tempGun = true;
            tempGunNum = 2;
        }
        if (other.gameObject.CompareTag("HeavyMat"))
        {
            this.GetComponent<PlayerController>().SetHeavyAllow();
            tempGun = true;
            tempGunNum = 3;
        }
        if (other.gameObject.CompareTag("RocketMat"))
        {
            this.GetComponent<PlayerController>().SetRocketAllow();
            tempGun = true;
            tempGunNum = 4;
        }

        if (other.gameObject.CompareTag("Enemy"))
        {
            //Debug.Log("hit by enemy in trigger");
            //less one to the current value of our hp count variable.
            countHP = countHP - 5; //deal 5 dmg if player hit by enemy hitbox
            PlayerSource.clip = PlayerHit;
            AudioSource.PlayClipAtPoint(PlayerSource.clip, transform.position);
            healthBar.fillAmount = ((float)countHP / (float)HealthMax);
            takeDamage = true;

        }
        //Update the currently displayed count by calling the SetCountText function.
        SetCountText();
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("OnCollision in Health_o2");
        if (collision.gameObject.CompareTag("EnemyBullet"))
        {
            //Debug.Log("hit by enemy bullet in collision");
            countHP = countHP - collision.gameObject.GetComponent<EnemyBullet>().damage;
            PlayerSource.clip = PlayerHit;
            AudioSource.PlayClipAtPoint(PlayerSource.clip, transform.position);
            healthBar.fillAmount = ((float)countHP / (float)HealthMax);
            takeDamage = true;

        }
        else if (collision.gameObject.CompareTag("Laser"))
        {
            //Debug.Log("hit by enemy bullet in collision");
            countHP = countHP - 10;
            PlayerSource.clip = PlayerHit;
            AudioSource.PlayClipAtPoint(PlayerSource.clip, transform.position);
            healthBar.fillAmount = ((float)countHP / (float)HealthMax);
            takeDamage = true;

        }
        else if (collision.gameObject.CompareTag("Enemy"))
        {
            //Debug.Log("hit by enemy in trigger");
            //less one to the current value of our hp count variable.
            countHP = countHP - 5; //deal 5 dmg if player hit by enemy hitbox
            PlayerSource.clip = PlayerHit;
            AudioSource.PlayClipAtPoint(PlayerSource.clip, transform.position);
            healthBar.fillAmount = ((float)countHP / (float)HealthMax);
            takeDamage = true;

        }

        SetCountText();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("NoAir"))
            SetCountText();
        if (other.gameObject.CompareTag("Air"))
        {
            if (maskOn.enabled == true)
            {
                CancelInvoke("RecoverOxygen");
            }
            if (maskOn.enabled == false)
            {
                InvokeRepeating("RecoverOxygen", 1.0f, 1f);
            }
            SetCountText();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("NoAir"))
            CancelInvoke("DecreaseOxygen");
        if (other.gameObject.CompareTag("Air"))
        {
            CancelInvoke("DecreaseOxygenNormal");
            CancelInvoke("RecoverOxygen");
        }
    }

    IEnumerator Example()
    {
        print(Time.time);
        yield return new WaitForSecondsRealtime(5);
        print(Time.time);
    }

    //This function updates the text displaying the number of objects we've collected and displays our victory message if we've collected all of them.
    void SetCountText()
    {
        //Set the text property of our our countText object to "Count: " followed by the number stored in our count variable.
        //countText.text = "Collectables: " + count.ToString() + "/" + numtowin.ToString();
        lifeText.text = countHP.ToString() + "/" + HealthMax.ToString();
        OxygenText.text = countOx.ToString() + "/" + OxygenMax.ToString();
        MoneyText.text = countMoney.ToString();
        maskState.text = "Helmet: " + maskOnOff;
        Lives.text = "Lives: " + playerLives.ToString();
        oxygenBar.fillAmount = ((float)countOx / (float)OxygenMax);
        healthBar.fillAmount = ((float)countHP / (float)HealthMax);
        IsDead();//letscheck
    }

    public void Kill()
    {
        countHP = 0;
        SetCountText();
    }

    //decreasing the o2 ammounts respectively
    void DecreaseOxygen()
    {
        if (countOx > 0 && maskOn.enabled == true)
        {
            countOx -= 1;
            oxygenBar.fillAmount = ((float)countOx / (float)OxygenMax);
        }
        else if (countOx > 0 && maskOn.enabled == false)
        {
            countOx -= 3;
            oxygenBar.fillAmount = ((float)countOx / (float)OxygenMax);
        }
    }

    //if mask is not taken off
    void DecreaseOxygenNormal()
    {
        if (countOx > 0 && maskOn.enabled == true)
        {
            countOx -= 1;
            oxygenBar.fillAmount = ((float)countOx / (float)OxygenMax);
        }
    }

    void RecoverOxygen()
    {
        if (countOx < OxygenMax && maskOn.enabled == false)
        {
            countOx += 1;
            oxygenBar.fillAmount = ((float)countOx / (float)OxygenMax);
        }
    }

    private void IsDead()
    {
        if (countHP <= 0)
        {
            playerLives -= 1;         //takes away life if dead
            SubtractMoney(tempMoney); //takes away money from stage if dead
            TempGunHandler();         //takes away gun for not clearing stage
            CancelInvoke("DecreaseOxygen");
            CancelInvoke("DecreaseOxygenNormal");
            anim.SetBool(a_dead_hash, true);
            MusicSource.clip = MusicClip;
            dead.enabled = true;
            AudioSource.PlayClipAtPoint(MusicClip, transform.position);
            //var particle = Instantiate(effect, transform.position, transform.rotation);//spawns at location
            Debug.Log("Player Died: No HP");
            SavePlayer();
            Destroy(this.GetComponent<PlayerController>());
                        Destroy(this.GetComponent<Health_O2_Music>());
        }
        if (countOx <= 0)
        {
            playerLives -= 1;         //takes away life if dead
            SubtractMoney(tempMoney); //takes away money from stage if dead
            TempGunHandler();         //takes away gun for not clearing stage
            CancelInvoke("DecreaseOxygen");
            CancelInvoke("DecreaseOxygenNormal");
            anim.SetBool(a_noair_hash, true);
            MusicSource.clip = MusicClip;
            dead.enabled = true;
            AudioSource.PlayClipAtPoint(MusicClip, transform.position);
            //var particle = Instantiate(effect, transform.position, transform.rotation);//spawns at location
            Debug.Log("Player Died: No O2");
            SavePlayer();
            Destroy(this.GetComponent<PlayerController>());
            Destroy(this.GetComponent<Health_O2_Music>());
        }
    }

    public void TakeExplosion(int damage)
    {
        Debug.Log("Player hit by explosion");

        countHP = countHP - damage;
        healthBar.fillAmount = ((float)countHP / (float)HealthMax);
        takeDamage = true;

        SetCountText();
    }

    public void ScreenFlashOnDamage()
    {
        if (takeDamage)
        {
            Color opaque = new Color(redFlash.color.r, 0f, 0f, 0.5f);
            redFlash.color = Color.Lerp(redFlash.color, opaque, 20 * Time.deltaTime);
            if (redFlash.color.a >= 0.4)
            {
                takeDamage = false;
            }
        }
        else //takeDamage = false
        {
            Color transparent = new Color(redFlash.color.r, 0f, 0f, 0f);
            redFlash.color = Color.Lerp(redFlash.color, transparent, 20 * Time.deltaTime);
        }
    }

    public void GameOver()
    {
        if (playerLives == 0)
        {
            //end game
            //reallocations to make sure player is reset properly.
            countMoney = 0;
            playerLives = 5;
            SavePlayer();
            SceneManager.LoadScene(0);
        }
    }

    public int MoneyCheck()
    {
        return countMoney;
    }

    public int LivesCheck()
    {
        return playerLives;
    }

    public int MaxHealthCheck()
    {
        return HealthMax;
    }

    public int MaxOxygenCheck()
    {
        return OxygenMax;
    }

    public void SubtractMoney(int i)
    {
        countMoney -= i;
    }

    public void ContractComplete(int i)
    {
        countMoney += i;
    }

    public void AddLife()
    {
        playerLives += 1;
    }

    public void HealthUpgrade()
    {
        countHP += 25;
        countHP = HealthMax;
        SetCountText();
    }

    public void OxygenUpgrade()
    {
        countOx += 25;
        countOx = OxygenMax;
        SetCountText();
    }

    public void TempGunHandler() //makes it so you don't keep gun if you die.
    {
        if(tempGun == true && tempGunNum != 0)
        {
            if(tempGunNum == 1)
            {
                this.GetComponent<PlayerController>().SetTrbFail();
            }
            if (tempGunNum == 2)
            {
                this.GetComponent<PlayerController>().SetShotgunFail();
            }
            if (tempGunNum == 3)
            {
                this.GetComponent<PlayerController>().SetHeavyFail();
            }
            if (tempGunNum == 4)
            {
                this.GetComponent<PlayerController>().SetRocketFail();
            }
        }
    }


    //save function & load function
    public void SavePlayer()
    {
        SaveSystem.SavePlayer(this);
    }

    public void LoadPlayer()
    {
        PlayerData data = SaveSystem.LoadPlayer();
        countMoney = data.money;
        playerLives = data.lives;
        HealthMax = data.maxHealth;
        OxygenMax = data.maxOxygen;
    }
}
