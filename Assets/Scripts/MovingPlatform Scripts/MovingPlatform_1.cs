﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 NOTE: Moving Platforms scripts should be unique to each platform, set the platform in the editor at its rightmost position,
        then you must set end distance manually in the public float endOffset to set the left bound
     */

public class MovingPlatform_1 : MonoBehaviour
{
    Vector3 origin;
    Vector3 end;
    public float endOffset;

    public bool movingRight;

    [Range(1, 10)]
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        origin = transform.position;
        end = origin + new Vector3(-endOffset, 0f, 0f);
        movingRight = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(movingRight)
        {
            transform.Translate(Vector3.right * speed * Time.deltaTime);

            if (transform.position.x >= origin.x)
                movingRight = false;
                
        }
        else
        {
            transform.Translate(Vector3.left * speed * Time.deltaTime);
            if (transform.position.x <= end.x)
                movingRight = true;
        }
    }
}
