﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleRotation : MonoBehaviour
{
    public float maxRotation = 45f;
    public float speed = 1f;

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Euler(0f, maxRotation * Mathf.Sin(Time.time * speed), 0f);
    }
}
