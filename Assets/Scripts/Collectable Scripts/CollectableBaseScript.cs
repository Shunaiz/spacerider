﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableBaseScript : MonoBehaviour
{
    public GameObject target;
    public ParticleSystem effect;


    // Your audio clip
    public AudioClip MusicClip;

    // the component that Unity uses to play your clip
    public AudioSource MusicSource;

    public int rotateSpeed = 30;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(rotateSpeed * Time.deltaTime, 0, 0);//rotates object (x,y,z)
        //move accordingly to what object needs.
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            AudioSource.PlayClipAtPoint(MusicClip, transform.position);
            gameObject.SetActive(false);
        }
    }
}
