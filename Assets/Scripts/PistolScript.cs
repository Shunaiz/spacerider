﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolScript : MonoBehaviour
{
    public float fireRate = 0; //if 0 single fire weapon
    public float damage = 10;
    public LayerMask whatToHit; //set to not player so he won't hit himself
    public Transform bulletPrefab;
    //public float fixDegree = 0;

    float timeToFire = 0;
    Transform FirePos;

    // Start is called before the first frame update
    void Awake()
    {
        FirePos = transform.Find("FirePos");
        if (FirePos == null)
        {
            Debug.LogError("No Fireposition");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (System.Math.Abs(fireRate) < Mathf.Epsilon)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                Shoot();
            }
        }
        else
        {
            if (Input.GetButtonDown("Fire1") && Time.time > timeToFire)
            {
                timeToFire = Time.time + 1 / fireRate;
                Shoot();
            }
        }
    }

    void Shoot()
    {
        //Debug.Log("Test");
        Vector2 mousePosition = new Vector2(Camera.main.ScreenToWorldPoint (Input.mousePosition).x, Camera.main.ScreenToWorldPoint (Input.mousePosition).y);
        Vector2 firePointPosition = new Vector2(FirePos.position.x, FirePos.position.y);
        //RaycastHit2D hit = Physics2D.Raycast(firePointPosition, mousePosition - firePointPosition, 100, whatToHit); //the direction
        Debug.DrawLine(firePointPosition, (mousePosition-firePointPosition)*100);
        Vector2 spawnLocation = firePointPosition;

        float AngleRad = Mathf.Atan2(mousePosition.y - firePointPosition.y , mousePosition.x - firePointPosition.x);

        float AngleDeg = (180 / Mathf.PI) * AngleRad;
        Instantiate(bulletPrefab, spawnLocation, Quaternion.Euler(0,0,(AngleDeg - 90)));

        //if (hit.collider != null)
        //{
            //Debug.DrawLine(firePointPosition, hit.point, Color.red);
        //}
        //Debug.Log("We hit" + hit.collider.name + "and did" + damage + "damage,");
    }
}
