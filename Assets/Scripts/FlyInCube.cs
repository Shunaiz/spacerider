﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyInCube : MonoBehaviour
{
    private Vector3 initialPos;
    private Quaternion initialRot;
    private Vector3 flyOffPos;
    // Start is called before the first frame update
    void Start()
    {
        initialPos = transform.position;
        initialRot = transform.rotation;
        flyOffPos = Random.onUnitSphere;
    }

    // Update is called once per frame
    void Update()
    {
        Transform playerTrans = GameObject.FindGameObjectWithTag("Player").transform;
        float distance = Vector3.Distance(playerTrans.position, initialPos);

        transform.position = initialPos + (flyOffPos + new Vector3(Mathf.Sin(Time.time + initialPos.x) * 0.025f, Mathf.Cos(Time.time * 2.0f + initialPos.z) * 0.025f, 0)) * Mathf.Clamp(distance - 5.0f, 0, 10);
    }
}
