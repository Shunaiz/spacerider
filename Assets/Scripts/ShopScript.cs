﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopScript : MonoBehaviour
{
    private int buttonWidth;
    private int buttonHeight;
    private int origin_x;
    private int origin_y;
    public GUISkin skin;
    public GameObject player;

    private static bool isPaused = false;
    public bool flag = false;
    // Use this for initialization
    void Start()
    {
        buttonWidth = 600;
        buttonHeight = 30;
        origin_x = Screen.width / 2 - buttonWidth / 2;
        origin_y = Screen.height / 2 - buttonHeight * 2;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
            isPaused = true;
    }

    void OnGUI()
    {
        //GUI.skin = skin;// Resources.Load<GUISkin>("Materials and Textures/UI/UISkin");

        if (isPaused==true && flag==false)
        {
            if (GUI.Button(new Rect(origin_x, origin_y, buttonWidth, buttonHeight), "Life ... 10,000$"))
            { //check if player has enough money allow if not no deal
                if (player.GetComponent<Health_O2_Music>().MoneyCheck() >= 10000)
                {
                    player.GetComponent<Health_O2_Music>().AddLife();
                    player.GetComponent<Health_O2_Music>().SubtractMoney(10000);
                    player.GetComponent<Health_O2_Music>().SavePlayer();
                }
                else
                {

                    //do nothing
                    flag = true;
                    
                    Debug.Log("Not Enough Money");
                    OnGUI();
                }
            }
            if (GUI.Button(new Rect(origin_x, origin_y + buttonHeight * 1 + 5, buttonWidth, buttonHeight), "Health Upgrade (+25) ... 5,000$"))
            { //check if player has enough money allow if not no deal
                if (player.GetComponent<Health_O2_Music>().MoneyCheck() >= 5000)
                {
                    player.GetComponent<Health_O2_Music>().HealthUpgrade();
                    player.GetComponent<Health_O2_Music>().SubtractMoney(5000);
                    player.GetComponent<Health_O2_Music>().SavePlayer();
                    player.GetComponent<PlayerController>().SaveGun();
                }
                else
                {
                    //do nothing
                    flag = true;

                    Debug.Log("Not Enough Money");
                    OnGUI();
                }
            }
            if (GUI.Button(new Rect(origin_x, origin_y + buttonHeight * 2 + 10, buttonWidth, buttonHeight), "Oxygen Upgrade (+25) ... 5,000$"))
            { //check if player has enough money allow if not no deal
                if (player.GetComponent<Health_O2_Music>().MoneyCheck() >= 5000)
                {
                    player.GetComponent<Health_O2_Music>().OxygenUpgrade();
                    player.GetComponent<Health_O2_Music>().SubtractMoney(5000);
                    player.GetComponent<Health_O2_Music>().SavePlayer();
                    player.GetComponent<PlayerController>().SaveGun();
                }
                else
                {
                    //do nothing
                    flag = true;

                    Debug.Log("Not Enough Money");
                    OnGUI();
                }
            }
            
            if (GUI.Button(new Rect(origin_x, origin_y + buttonHeight * 5 + 25, buttonWidth, buttonHeight), "Exit Shop....>>"))
            {
                isPaused = false;
            }
        }
        else if(isPaused == true && flag == true)
        {
            GUI.Button(new Rect(origin_x, origin_y, 200 , 70), "Not Enough Money"); 
            if(GUI.Button(new Rect(origin_x, origin_y+90, 200, 70), "Exit"))
            {
                flag = false;
                isPaused = false;
            }
        }

    }

    public static bool IsPaused()
    {
        if (isPaused)
            return true;
        else
            return false;
    }
}