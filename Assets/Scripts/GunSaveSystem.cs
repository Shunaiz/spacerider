﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class GunSaveSystem
{
    public static void SaveGun(PlayerController gun)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/gun.data";
        FileStream stream = new FileStream(path, FileMode.Create);

        GunData data = new GunData(gun);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static GunData LoadGun()
    {
        string path = Application.persistentDataPath + "/gun.data";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            GunData data = formatter.Deserialize(stream) as GunData;
            stream.Close();

            return data;
        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }
}
