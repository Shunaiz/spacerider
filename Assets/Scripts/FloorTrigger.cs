﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorTrigger : MonoBehaviour
{
    public UnityEngine.Events.UnityEvent floorTriggerEnter;
    public UnityEngine.Events.UnityEvent floorTriggerExit;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            //Debug.Log("Player entered trigger");
            floorTriggerEnter.Invoke();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            //Debug.Log("Player left trigger");
            floorTriggerExit.Invoke();
        }
    }
}
