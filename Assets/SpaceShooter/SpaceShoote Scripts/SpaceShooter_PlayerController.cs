﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SpaceShooter_PlayerController : MonoBehaviour
{
    public GameObject bullet;
    public Vector2 limits;
    private Vector3 originalPosition;


    //Robert Added Start===========================================
    private Rigidbody rb;
    [Range(1, 100)] //allows adjusting jump height while testing the game to get a specific feel
    public float dodgeSpeed = 40.0f;
    [Range(1, 100)] //allows adjusting jump height while testing the game to get a specific feel
    public float moveSpeed = 20.0f;
    [Range(0f, 2f)]
    public float boostTime;
    public Text lifeText;           //Store a reference to the UI Text component which will display Life
    public Text FuelText;
    public Text MoneyText;
    public RawImage dead;
    public AudioClip MusicClip;
    //public AudioClip StageMusic;
    public AudioSource MusicSource;
    private float timeElapsed;
    [Range(0.2f, 3f)]
    public float fireRate; //placeholder for fire rate - should be weapon dependent in the future
    //public ParticleSystem effect;
    public Transform FirePos;
    public Transform FirePos2;
    public float smooth = 1f;
    private Quaternion targetRotation;
    private bool speedUp = false;

    private Animator anim;
    private int a_right_hash = Animator.StringToHash("StarSparrowRight");
    private int a_left_hash = Animator.StringToHash("StarSparrowLeft");

    private Rigidbody2D rb2d;       //Store a reference to the Rigidbody2D component required to use 2D Physics.
    private int count;              //Integer to store the number of pickups collected so far.
    public int countHP;             //counter for Health
    private int countFuel;          //counter for Fuel
    private static int highScore = 0;   //counter for Gold/Money
    public Image healthBar;         //yeah
    public int HealthMax;           //set Max health for character hp bar
    public Image FuelBar;           //yeah
    public int FuelMax;             //set Max health for character hp bar
    //public ParticleSystem effect;
    //Robert Added End========================================

    // Start is called before the first frame update
    void Start()
    {
        originalPosition = transform.localPosition;

        //Robert Added Start===========================================
        //Initialize counts to zero
        countHP = 100;
        HealthMax = 100;
        countFuel = 50;
        FuelMax = 50;
        dead.enabled = false;
        timeElapsed = 0f;
        targetRotation = transform.rotation;
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        rb.interpolation = RigidbodyInterpolation.Interpolate; //smooths out player ship movement
        //Initialze winText to a blank string since we haven't won yet at beginning.
        //winText.text = "";
        //countText.text = "Collectables: " + count.ToString() + "/" + numtowin.ToString();
        lifeText.text = "HP: " + countHP.ToString() + "/100";
        FuelText.text = "Fuel: " + countFuel.ToString() + "/50";
        MoneyText.text = "HighScore: " + highScore.ToString();
        //MusicSource.clip = StageMusic;
        //AudioSource.PlayClipAtPoint(StageMusic, transform.position);

        //Call our SetCountText function which will update the text with the current value for count.
        SetCountText();
        //Robert Added End========================================
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseMenu.IsPaused()) return;
        timeElapsed += Time.deltaTime;

        //Count Space Enemies before advancing
        if (GameObject.FindGameObjectWithTag("Enemy") == null)
        {
            //transform.position = transform.position + new Vector3(0, 0, 5) * Time.deltaTime;
        }
        if (Input.GetKeyDown(KeyCode.S) && Input.GetKey(KeyCode.D) && countFuel > 9)
        {
            StartCoroutine("ExampleRight");
        }

        if (Input.GetKeyDown(KeyCode.S) && Input.GetKey(KeyCode.A) && countFuel > 9)
        {
            StartCoroutine("ExampleLeft");
        }
        SetCountText();
    }

    void FixedUpdate()
    {
        if (PauseMenu.IsPaused()) return;

        float distance;
        //var particle = Instantiate(effect, transform.position, transform.rotation);//spawns at location
        //transform.position = transform.position + new Vector3(0, 0, 5) * Time.deltaTime;

        //Player Controls
        //Input.GetAxis("Vertical");
        //transform.position = transform.position + new Vector3(Input.GetAxis("Horizontal"), 0, 0) * Time.deltaTime * 20;


        // Set y and z velocities to zero
        rb.velocity = new Vector3(rb.velocity.x, 0, 0);

        if (speedUp == false)
        {
            // Distance ( speed = distance / time --> distance = speed * time)
            distance = moveSpeed * Time.deltaTime;
        }
        else
        {
            distance = dodgeSpeed * Time.deltaTime;
        }
        // Input on x ("Horizontal")
        float hAxis = Input.GetAxis("Horizontal");

        // Movement vector
        Vector3 movement = new Vector3(hAxis * distance, 0f, 0f);

        // Current position
        Vector3 currPosition = transform.position;

        // New position
        Vector3 newPosition = currPosition + movement;

        // Move the rigid body
        rb.MovePosition(newPosition);

        if (Input.GetButtonDown("Fire1"))
        {
            FireHandler();
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(transform.localPosition, new Vector3(limits.x, 0, limits.y));
    }

    //Robert Added Start===========================================
    //OnTriggerEnter2D is called whenever this object overlaps with a trigger collider.
    void OnTriggerEnter(Collider other)
    {
        //Debug.Log("OnTrigger in Health_o2");
        //Check the provided Collider2D parameter other to see if it is tagged "PickUp", if it is...
        //(Robert) Changed this part to medkit pickup
        if (other.gameObject.CompareTag("Medkit"))
        {
            if (countHP >= 80 && countHP <= HealthMax)
            {
                //Debug.Log("grabbed a medkit for Full HP");
                //Add one to the current value of our count variable.
                countHP = HealthMax;
                healthBar.fillAmount = ((float)countHP / (float)HealthMax);
                //^^^adjusts health bar.
            }
            else if (countHP <= 79)
            {
                //Debug.Log("grabbed a medkit for 20HP");
                //Add one to the current value of our count variable.
                countHP = countHP + 20;
                healthBar.fillAmount = ((float)countHP / (float)HealthMax);
                //^^^adjusts health bar.
            }
            else
            { }
        }

        //Check the provided Collider2D parameter other to see if it is tagged "PickUp", if it is...
        //(Robert) Changed this part to Space Cash pickup
        if (other.gameObject.CompareTag("GoldBrick"))
        {
            //adds 500 for each brick of gold for now
            highScore = highScore + 500;
        }
        if (other.gameObject.CompareTag("EnemyBullet"))
        {
            //Debug.Log("hit by enemy bullet in trigger");
            //less one to the current value of our hp count variable.
            Destroy(other);
            countHP = countHP - 25;
            healthBar.fillAmount = ((float)countHP / (float)HealthMax);
            //^^^adjusts health bar.
        }
        //Update the currently displayed count by calling the SetCountText function.
        SetCountText();
    }

    IEnumerator ExampleRight()
    {
        print(Time.time);
        RotateShipRight();
        speedUp = true;
        yield return new WaitForSecondsRealtime(boostTime);
        speedUp = false;
        print(Time.time);
    }

    IEnumerator ExampleLeft()
    {
        print(Time.time);
        RotateShipLeft();
        speedUp = true;
        yield return new WaitForSecondsRealtime(boostTime);
        speedUp = false;
        print(Time.time);
    }
    //This function updates the text displaying the number of objects we've collected and displays our victory message if we've collected all of them.
    void SetCountText()
    {
        //Set the text property of our our countText object to "Count: " followed by the number stored in our count variable.
        //countText.text = "Collectables: " + count.ToString() + "/" + numtowin.ToString();
        lifeText.text = "HP: " + countHP.ToString() + "/100";
        FuelText.text = "Fuel: " + countFuel.ToString() + "/50";
        MoneyText.text = "Highscore: " + highScore.ToString();

        if (countHP <= 0)
        {
           
            dead.enabled = true;
            MusicSource.clip = MusicClip;
            AudioSource.PlayClipAtPoint(MusicClip, transform.position);
            //var particle = Instantiate(effect, transform.position, transform.rotation);//spawns at location
            Debug.Log("player dead");
            gameObject.SetActive(false);
        }
    }

    void FireHandler()
    {
        if (timeElapsed > fireRate)
        {

            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                GameObject firedBullet = Instantiate(bullet, FirePos.position + new Vector3(0, 0, 4), transform.rotation);
                GameObject firedBullet2 = Instantiate(bullet, FirePos2.position + new Vector3(0, 0, 4), transform.rotation);
                //firedBullet.GetComponent<SpaceShooter_Bullet>().isEnemyBullet = false;
                timeElapsed = 0f;
            }
        }
        else return;
    }

    void RotateShipRight()
    {
        anim.SetTrigger(a_right_hash);

        Vector3 dodgeVector = new Vector3(dodgeSpeed, 0f, 0f);

        rb.velocity = rb.velocity + dodgeVector;

        //transform.position = transform.position + new Vector3(Input.GetAxis("Horizontal") + 40, 0, 0) * Time.deltaTime * 20;
        countFuel = countFuel - 10;
        FuelBar.fillAmount = ((float)countFuel / (float)FuelMax);
        //targetRotation *= Quaternion.AngleAxis(10, Vector3.back);
        //transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, 1);
    }

    void RotateShipLeft()
    {
        anim.SetTrigger(a_left_hash);

        Vector3 dodgeVector = new Vector3(-dodgeSpeed, 0f, 0f);

        rb.velocity = rb.velocity + dodgeVector;

        //transform.position = transform.position + new Vector3(Input.GetAxis("Horizontal") - 40, 0, 0) * Time.deltaTime * 20;
        countFuel = countFuel - 10;
        FuelBar.fillAmount = ((float)countFuel / (float)FuelMax);
        //targetRotation *= Quaternion.AngleAxis(10, Vector3.forward);
        //transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, 1);
    }

    IEnumerator ReplenishFuel()
    {
        yield return new WaitForSecondsRealtime(5);
        countFuel += 10;
        FuelBar.fillAmount = ((float)countFuel / (float)FuelMax);
    }
    //Robert Added End========================================
}


