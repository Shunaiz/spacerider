﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShooter_Bullet : MonoBehaviour
{
    public bool isEnemyBullet;
    public float lifeTime = 15;
    public Vector3 velocity;
    public float damage = 10;
    //public float attraction = 0;
    private GameObject closestTarget;

    // Start is called before the first frame update
    void Start()
    {
        closestTarget = GameObject.FindGameObjectWithTag(isEnemyBullet ? "Player" : "Enemy");
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseMenu.IsPaused()) return;

        velocity.y = 0;
        lifeTime -= Time.deltaTime;
        if(lifeTime < 0)
            Destroy(gameObject);
        
        transform.position = transform.position + velocity * Time.deltaTime;
        transform.LookAt(Camera.main.transform.position, -Vector3.up);
    }

    enum OtherType { PLAYER, ENEMY, OTHER };
    void OnTriggerEnter(Collider collision)
    {
        //Find type of other colliding object
        OtherType type = OtherType.OTHER;
        if (collision.gameObject.GetComponent<SpaceShooter_PlayerController>() != null)
            type = OtherType.PLAYER;
        else if (collision.gameObject.GetComponent<SpaceShooter_EnemyController>() != null)
            type = OtherType.ENEMY;
        
        //If we're colliding something unassigned, just explode
        if(type == OtherType.OTHER)
        {
            Debug.Log("Collided with other");
            Destroy(gameObject);
            return;
        }

        if(isEnemyBullet && type == OtherType.PLAYER)
        {
            //Player is Hit! hurt the player
        }
        else if(!isEnemyBullet && type == OtherType.ENEMY)
        {
            //Enemy is Hit! Destroy them!
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
    }
}
