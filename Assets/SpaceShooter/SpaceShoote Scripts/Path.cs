﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Path : MonoBehaviour {
    void OnDrawGizmos() {
        Gizmos.color = Color.white;

        for (int i = 1; i < transform.childCount; i++) {
            Gizmos.DrawLine(transform.GetChild(i-1).position, transform.GetChild(i).position);
        }


    }


}