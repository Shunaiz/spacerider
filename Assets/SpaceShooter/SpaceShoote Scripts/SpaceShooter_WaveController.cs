﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpaceShooter_WaveController : MonoBehaviour
{
    public int numShips = 5;
    public int numRows = 5;
    public GameObject motherShip;
    public Transform spawnPoint;
    public GameObject enemyPrefab;
    public GameObject[] paths;
    public int maxActive = 3;

    private float lastActivated = 5;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseMenu.IsPaused()) return;

        lastActivated += Time.deltaTime;

        //Check if player is dead
        if (GameObject.FindGameObjectWithTag("Player") == null)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        //Check if all enemies are dead
        if (GameObject.FindGameObjectWithTag("Enemy") == null)
        {
            SceneManager.LoadScene(1);
        }

        //Check the state of all Enemies
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        int active = 0;
        foreach(GameObject enemy in enemies) {
            if (enemy.GetComponent<SpaceShooter_EnemyController>().isActive())
                active++;
        }

        foreach (GameObject enemy in enemies)
        {
            if (!enemy.GetComponent<SpaceShooter_EnemyController>().isActive() && active < maxActive && lastActivated > 5)
            {
                enemy.GetComponent<SpaceShooter_EnemyController>().StartAttack(paths[Random.Range(0, paths.Length - 1)]);
                lastActivated = 0;
                active++;
                break;
            }
        }
    }
}
