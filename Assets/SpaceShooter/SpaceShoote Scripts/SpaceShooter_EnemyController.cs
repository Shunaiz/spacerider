﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShooter_EnemyController : MonoBehaviour
{
    public float speed = 5f;
    public float closeEnough = 0.5f;
    public GameObject bullet;

    public GameObject pathToFollow;
    private Vector3 startPosition;
    private Quaternion startRotation;
    private int point = -1;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        startRotation = transform.rotation;
    }

    public void StartAttack(GameObject path)
    {
        pathToFollow = path;
        point = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseMenu.IsPaused()) return;

        //Path Follow
        Vector3 nextPosition;
        Quaternion nextRotation;
        if (point >= 0)
        {
            nextPosition = pathToFollow.transform.GetChild(point).position;
            nextRotation = pathToFollow.transform.GetChild(point).rotation;
        } else
        {
            nextPosition = startPosition;
            nextRotation = startRotation;
        }
        Quaternion rotation = Quaternion.LookRotation(nextPosition - transform.position, transform.TransformDirection(new Vector3(0,0,0)));
        transform.rotation = rotation;
        float dist = Vector3.Distance(nextPosition, transform.position);
        transform.position = Vector3.MoveTowards(transform.position, nextPosition, Time.deltaTime * speed);
        transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        if (Vector3.Distance(this.transform.position, nextPosition) < closeEnough) {
            if(point >= 0)
                point++;
            if (point >= pathToFollow.transform.childCount)
                point = -1;
        }

        //Shooting Script
        if ((Time.frameCount % 180 == 0 || Time.frameCount % 180 == 20 || Time.frameCount % 180 == 40 || Time.frameCount % 180 == 60 || Time.frameCount % 180 == 80 || Time.frameCount % 180 == 100) && point >= 0) {
            GameObject firedBullet = Instantiate(bullet, transform.position, transform.rotation);
        }
    }

    public bool isActive()
    {
        return point >= 0;
    }
}
