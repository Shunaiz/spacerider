﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShooter_Camera : MonoBehaviour
{
    public GameObject target;
    public float floatIntensity = 0.1f;
    private float shakeIntensity = 0.0f;

    public Vector3 origin;

    // Start is called before the first frame update
    void Start()
    {
        origin = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseMenu.IsPaused()) return;

        //Add motion
        Vector3 lookAt = target.transform.position;
        lookAt += new Vector3(Mathf.Sin(Time.time * 0.95f) * floatIntensity, Mathf.Cos(Time.time * 1.7f) * floatIntensity, Mathf.Cos(Time.time / 1.2f + 0.5f) * floatIntensity);

        transform.LookAt(lookAt);
    }

    public void ShakeCamera(float shakeIntensity)
    {

    }
}
